#include <stdio.h>

#include <cuda_runtime.h>
#include <cufft.h>

#include "pfb/PolyphaseFilterBankEngineCUDA_kernels.cuh"

__device__ unsigned roll (unsigned idx, unsigned size, unsigned roll_by)
{
  unsigned idx_rolled = idx + (size - roll_by);
  if (idx_rolled >= size) {
    return idx_rolled - size;
  } else {
    return idx_rolled;
  }
}

template<typename T>
__global__ void k_print_array(const T* arr, unsigned size)
{

  // const unsigned total_size_x = gridDim.x * blockDim.x;
  const unsigned idx = blockIdx.x*blockDim.x + threadIdx.x;

  if (idx == 0) {
    for (unsigned idat=idx; idat < size; idat ++) {
      printf("%f ", arr[idat]);
    }
    printf("\n");
  }

}

template __global__ void k_print_array<float>(const float* arr, unsigned size);


template<typename T>
__device__ T warp_reduce_sum_complex(T val) {
  for (unsigned offset = warpSize/2; offset > 0; offset >>= 1) {
    val.x += __shfl_down_sync(FULL_MASK, val.x, offset);
    val.y += __shfl_down_sync(FULL_MASK, val.y, offset);
  }
  return val;
}

template __device__ cufftComplex warp_reduce_sum_complex<
  cufftComplex>(cufftComplex val);
template __device__ cufftDoubleComplex warp_reduce_sum_complex<
  cufftDoubleComplex>(cufftDoubleComplex val);

template<typename T>
__global__ void k_apply_filter_reduce_sum (
  T* in,
  T* out,
  T* filter_coeff,
  unsigned filter_taps,
  unsigned step,
  unsigned downsample,
  unsigned nblocks,
  unsigned filter_taps_per_sample
)
{
  // amount of scratch space should be filter_taps_per_sample / warpSize
  extern __shared__ unsigned char _scratch[];
  T* scratch = reinterpret_cast<T*>(_scratch);

  const unsigned total_size_x = blockDim.x;
  const unsigned idx = threadIdx.x;
  const unsigned warp_idx = threadIdx.x % warpSize;
  const unsigned warp_num = threadIdx.x / warpSize;
  unsigned max_warp_num = total_size_x / warpSize;
  if (max_warp_num == 0) {
    max_warp_num++;
  }

  const unsigned total_size_block = gridDim.x; // for processing blocks
  const unsigned total_size_chan = gridDim.y; // for processing channels

  if (blockIdx.y >= downsample || blockIdx.x >= nblocks || idx >= filter_taps_per_sample) {
    return;
  }

  unsigned in_offset;
  unsigned out_offset;
  unsigned in_idx;
  unsigned roll_idx;
  T mul;
  T in_temp;
  T filter_temp;
  T partial_sum;

  for (unsigned ichan=blockIdx.y; ichan < downsample; ichan += total_size_chan) {
    for (unsigned iblock=blockIdx.x; iblock < nblocks; iblock += total_size_block) {
      in_offset = iblock*step;
      roll_idx = in_offset - (in_offset/downsample)*downsample;
      out_offset = iblock*downsample;
      partial_sum.x = 0; partial_sum.y = 0;
      for (unsigned s=idx; s < filter_taps_per_sample; s += total_size_x) {
        in_idx = ichan + s*downsample;
        in_idx = roll(in_idx, filter_taps, roll_idx);
        in_temp = in[in_idx + in_offset];
        filter_temp = filter_coeff[in_idx];
        mul.x = in_temp.x * filter_temp.x - in_temp.y * filter_temp.y;
        mul.y = in_temp.x * filter_temp.y + in_temp.y * filter_temp.x;
        partial_sum.x = partial_sum.x + mul.x;
        partial_sum.y = partial_sum.y + mul.y;
      }
      partial_sum = warp_reduce_sum_complex<T>(partial_sum);
      if (warp_idx == 0) {
        scratch[warp_num] = partial_sum;
      }

      __syncthreads();

      if (warp_num == 0) {
        if (warp_idx >= max_warp_num) {
          partial_sum.x = 0;
          partial_sum.y = 0;
        } else {
          partial_sum = scratch[warp_idx];
        }
        // partial_sum = scratch[warp_idx];
        partial_sum = warp_reduce_sum_complex<T>(partial_sum);

        if (warp_idx == 0) {
          out[ichan + out_offset].x = downsample * partial_sum.x;
          out[ichan + out_offset].y = downsample * partial_sum.y;
        }
      }
    }
  }
}


template __global__ void k_apply_filter_reduce_sum<cufftComplex> (
  cufftComplex* in,
  cufftComplex* out,
  cufftComplex* filter_coeff,
  unsigned filter_taps,
  unsigned step,
  unsigned downsample,
  unsigned nblocks,
  unsigned filter_taps_per_sample
);

template __global__ void k_apply_filter_reduce_sum<cufftDoubleComplex> (
  cufftDoubleComplex* in,
  cufftDoubleComplex* out,
  cufftDoubleComplex* filter_coeff,
  unsigned filter_taps,
  unsigned step,
  unsigned downsample,
  unsigned nblocks,
  unsigned filter_taps_per_sample
);


template<typename T>
__global__ void k_apply_filter (
  T* in,
  T* out,
  T* filter_coeff,
  unsigned filter_taps,
  unsigned step,
  unsigned downsample,
  unsigned nblocks,
  unsigned filter_taps_per_sample
)
{
  const unsigned total_size_x = blockDim.x * gridDim.x; // for processing blocks
  const unsigned total_size_y = blockDim.y * gridDim.y; // for processing channels
  const unsigned idx = blockIdx.x*blockDim.x + threadIdx.x;
  const unsigned idy = blockIdx.y;

  if (idy >= downsample || idx >= nblocks) {
    return;
  }

  unsigned in_offset;
  unsigned out_offset;
  unsigned in_idx;
  unsigned roll_idx;
  T mul;
  T in_temp;
  T filter_temp;

  for (unsigned ichan=idy; ichan < downsample; ichan += total_size_y) {
    for (unsigned iblock=idx; iblock < nblocks; iblock += total_size_x) {
      in_offset = iblock*step;
      roll_idx = in_offset - (in_offset/downsample)*downsample;
      out_offset = iblock*downsample;
      for (unsigned s=0; s<filter_taps_per_sample; s++) {
        in_idx = ichan + s*downsample;
        in_idx = roll(in_idx, filter_taps, roll_idx);
        in_temp = in[in_idx + in_offset];
        filter_temp = filter_coeff[in_idx];
        mul.x = in_temp.x * filter_temp.x - in_temp.y * filter_temp.y;
        mul.y = in_temp.x * filter_temp.y + in_temp.y * filter_temp.x;
        out[ichan + out_offset].x = out[ichan + out_offset].x + mul.x;
        out[ichan + out_offset].y = out[ichan + out_offset].y + mul.y;
      }
      out[ichan + out_offset].x = downsample * out[ichan + out_offset].x;
      out[ichan + out_offset].y = downsample * out[ichan + out_offset].y;
    }
  }
}

template __global__ void k_apply_filter<cufftComplex> (
  cufftComplex* in,
  cufftComplex* out,
  cufftComplex* filter_coeff,
  unsigned filter_taps,
  unsigned step,
  unsigned downsample,
  unsigned nblocks,
  unsigned filter_taps_per_sample);

template __global__ void k_apply_filter<cufftDoubleComplex> (
  cufftDoubleComplex* in,
  cufftDoubleComplex* out,
  cufftDoubleComplex* filter_coeff,
  unsigned filter_taps,
  unsigned step,
  unsigned downsample,
  unsigned nblocks,
  unsigned filter_taps_per_sample);
