#include <iostream>

#if HAVE_CUDA
#include <cuda_runtime.h>
#endif

#include "pfb/util/util.hpp"


namespace pfb {
namespace util {

  unsigned calc_filter_taps_padded (
    unsigned channels,
    unsigned filter_taps
  )
  {
    unsigned rem = filter_taps % channels;
    unsigned diff = 0;
    if (rem != 0) {
      diff = channels - rem;
    }
    return filter_taps + diff;
  }

  four_tuple calc_out_size (
    unsigned channels,
    const rational& os_factor,
    unsigned in_size,
    unsigned filter_taps
  )
  {
    unsigned filter_taps_padded = calc_filter_taps_padded(channels, filter_taps);
    unsigned step = os_factor.de * channels / os_factor.nu;
    unsigned nblocks = (in_size - filter_taps_padded) / step;
    unsigned out_size = nblocks * channels;
    return std::make_tuple(filter_taps_padded, step, nblocks, out_size);
  }

  void check_error (const std::string& msg)
  {
  #if HAVE_CUDA
    cudaDeviceSynchronize ();

    cudaError error = cudaGetLastError ();
    if (error != cudaSuccess) {
      std::cerr << "Error: " <<  msg << ": " << error << ", "
        << cudaGetErrorString (error) << std::endl;
    }
  #endif
  }

}
}
