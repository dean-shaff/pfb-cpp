#ifndef __create_parser_hpp
#define __create_parser_hpp
#include "cxxopts.hpp"

namespace pfb {
namespace internal {
  inline void create_parser (cxxopts::Options& options)
  {
    options.add_options()
      ("v,verbose", "Verbose output")
      ("h,help", "Help")
      ("i,in_file", "Path to input file")
      ("f,fir_filter_file", "Path to FIR filter coefficients")
      ("c,channels", "Number of channels to create")
      ("gpu", "Use GPU engine")
      ;
  }
}
}
#endif
