#ifndef __DADAFile_hpp
#define __DADAFile_hpp

#include <string>
#include <sstream>
#include <vector>
#include <map>

namespace pfb {
namespace internal {

class DADAFile {

  using MapType = std::map<std::string, std::string>;

public:

  DADAFile (const std::string& file_path);

  DADAFile& load()

  void dump();

  template<typename Type>
  Type* data() {
    return reinterpret_cast<Type*>(bytes);
  }

  const std::string get_file_path const () { return file_path; }

private:
  std::string file_path;
  MapType header;
  void* bytes;

  static const unsigned default_header_size = 4096;

  void load (unsigned header_size) {

  }

  template<typename T>
  void test::util::load_binary_data (std::string file_path, std::vector<T>& test_data)
  {
    std::streampos size;

    std::ifstream file (file_path, std::ios::in|std::ios::binary|std::ios::ate);
    if (file.is_open())
    {
      file.seekg(0, std::ios::end);
      size = file.tellg();
      file.seekg(0, std::ios::beg);

      // read the data:
      std::vector<char> file_bytes(size);
      file.read(&file_bytes[0], size);
      file.close();

      unsigned T_size = (size / sizeof(T));
      // std::cerr << "T_size=" << T_size << std::endl;

      const T* data = reinterpret_cast<const T*>(file_bytes.data());
      test_data.assign(data, data + T_size);
    }
  }

  std::string header_to_str(MapType& header_map)
  {
    std::stringstream sstr;

    int header_size = std::stoi(header_map["HDR_SIZE"]);

    for (auto it=header_map.begin(); it != header_map.end(); it++) {
      sstr << it->first << " " << it->second << "\n";
    }

    int bytes_remaining = header_size - sstr.str().size();

    for (int idx=0; idx<bytes_remaining; idx++) {
      sstr << '\0';
    }

    return sstr.str();
  }


}


}
}


#endif
