
#include "pfb/PolyphaseFilterBankPlan.hpp"
#if HAVE_CUDA
#include "pfb/PolyphaseFilterBankEngineCUDA.hpp"
#endif
#include "pfb/internal/create_parser.hpp"


int main (int argc, char *argv[]) {
  cxxopts::Options options(
    "pfb_dada",
    "Channelize data contained in DADA file");

  pfb::internal::create_parser(options);

  auto result = options.parse(argc, argv);

  if (result["help"].as<bool>()) {
    std::cerr << options.help() << std::endl;
    return 0;
  }



}
