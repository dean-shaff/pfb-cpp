#include "pfb/PolyphaseFilterBankEngineCPU.hpp"

namespace pfb {

template<typename dtype>
PolyphaseFilterBankEngineCPU<dtype>::PolyphaseFilterBankEngineCPU (
  unsigned _channels,
  pfb::util::rational _os_factor,
  unsigned _in_size,
  unsigned _filter_taps,
  unsigned _step,
  unsigned _filter_taps_padded,
  unsigned _out_size,
  unsigned _nblocks
) : PolyphaseFilterBankEngine<dtype>(_channels, _os_factor, _in_size, _filter_taps, _step, _filter_taps_padded, _out_size, _nblocks)
{
  #if VERBOSE
    std::cerr << "pfb::PolyphaseFilterBankEngineCPU::PolyphaseFilterBankEngineCPU" << std::endl;
  #endif
}

template<typename dtype>
void PolyphaseFilterBankEngineCPU<dtype>::setup (
  dtype* _output
)
{
  #if VERBOSE
    std::cerr << "pfb::PolyphaseFilterBankEngineCPU::setup" << std::endl;
  #endif

  int rank = 1; // 1D transform
  int n[] = {(int) this->channels}; /* 1d transforms of length channels */
  int howmany = (int) this->nblocks;
  int idist = (int) this->channels;
  int odist = (int) this->channels;
  int istride = 1;
  int ostride = 1;
  int *inembed = n;
  int *onembed = n;

  #if VERBOSE
    std::cerr << "pfb::PolyphaseFilterBankEngineCPU::setup rank=" << rank << std::endl;
    std::cerr << "pfb::PolyphaseFilterBankEngineCPU::setup n=" << n[0] << std::endl;
    std::cerr << "pfb::PolyphaseFilterBankEngineCPU::setup howmany=" << howmany << std::endl;
    std::cerr << "pfb::PolyphaseFilterBankEngineCPU::setup idist=" << idist << std::endl;
    std::cerr << "pfb::PolyphaseFilterBankEngineCPU::setup odist=" << odist << std::endl;
    std::cerr << "pfb::PolyphaseFilterBankEngineCPU::setup istride=" << istride << std::endl;
    std::cerr << "pfb::PolyphaseFilterBankEngineCPU::setup ostride=" << ostride << std::endl;
    std::cerr << "pfb::PolyphaseFilterBankEngineCPU::setup inembed=" << *inembed << std::endl;
    std::cerr << "pfb::PolyphaseFilterBankEngineCPU::setup onembed=" << *onembed << std::endl;
  #endif


  typename type_map::type* in_fft = reinterpret_cast<typename type_map::type*>(_output);

  plan = type_map::plan_many_dft(
    rank, n, howmany,
    in_fft, inembed, istride, idist,
    in_fft, onembed, ostride, odist,
    FFTW_FORWARD, FFTW_ESTIMATE);

  this->is_setup = true;
}


template<typename dtype>
void PolyphaseFilterBankEngineCPU<dtype>::operator() (
  dtype* _input, dtype* _filter_coeff, dtype* _output
)
{
  #if VERBOSE
    std::cerr << "pfb::PolyphaseFilterBankEngineCPU::operator()" << std::endl;
  #endif

  apply_filter(
    _input, _filter_coeff, _output
  );

  type_map::execute(plan);
}




template<typename dtype>
void PolyphaseFilterBankEngineCPU<dtype>::apply_filter(
  dtype* input,
  dtype* filter_coeff,
  dtype* output
)
{
  #if VERBOSE
    std::cerr << "pfb::PolyphaseFilterBankEngineCPU::apply_filter" << std::endl;
  #endif

  auto roll = [] (int idx, int size, int roll_by) -> int {
    int idx_rolled = idx + (size - roll_by);
    if (idx_rolled >= size) {
      return idx_rolled - size;
    } else {
      return idx_rolled;
    }
  };

  unsigned taps_per_sample = this->filter_taps_padded / this->channels;

  dtype* in_ptr = input;
  dtype* out_ptr = output;

  unsigned block_step; // size of step in input samples
  unsigned roll_idx; // this determines by how much we roll ``idx``
  unsigned idx; // the index of the input and filter coefficients buffers

  // #pragma omp parallel for
  for (unsigned iblock=0; iblock<this->nblocks; iblock++)
  {
    block_step = iblock*this->step;
    roll_idx = block_step - (block_step/this->channels)*this->channels;

    for (unsigned ichan=0; ichan<this->channels; ichan++) {
      for (unsigned i=0; i<taps_per_sample; i++) {
        idx = ichan + i*this->channels;
        idx = roll(idx, this->filter_taps_padded, roll_idx);
        out_ptr[ichan] += in_ptr[idx] * filter_coeff[idx];
      }
    }
    for (unsigned ichan=0; ichan<this->channels; ichan++) {
      out_ptr[ichan] *= this->channels;
    }
    in_ptr += this->step;
    out_ptr += this->channels;
  }
}




template class PolyphaseFilterBankEngineCPU<std::complex<float>>;
template class PolyphaseFilterBankEngineCPU<std::complex<double>>;
}
