#include "pfb/util/util.hpp"
#include "pfb/PolyphaseFilterBankEngineCUDA.hpp"
#include "pfb/PolyphaseFilterBankEngineCUDA_kernels.cuh"


namespace pfb {

template<typename dtype>
PolyphaseFilterBankEngineCUDA<dtype>::PolyphaseFilterBankEngineCUDA (
  unsigned _channels,
  pfb::util::rational _os_factor,
  unsigned _in_size,
  unsigned _filter_taps,
  unsigned _step,
  unsigned _filter_taps_padded,
  unsigned _out_size,
  unsigned _nblocks
) : PolyphaseFilterBankEngine<dtype>(_channels, _os_factor, _in_size, _filter_taps, _step, _filter_taps_padded, _out_size, _nblocks)
{
  #if VERBOSE
    std::cerr << "pfb::PolyphaseFilterBankEngineCUDA::PolyphaseFilterBankEngineCUDA" << std::endl;
  #endif
}

template<typename dtype>
void PolyphaseFilterBankEngineCUDA<dtype>::setup (
  dtype* _output
)
{
  #if VERBOSE
    std::cerr << "pfb::PolyphaseFilterBankEngineCUDA::setup" << std::endl;
  #endif
  int rank = 1; // 1D transform
  int n[] = {(int) this->channels}; /* 1d transforms of length 10 */
  int howmany = this->nblocks;
  int idist = this->channels;
  int odist = this->channels;
  int istride = 1;
  int ostride = 1;
  int *inembed = n, *onembed = n;

  cufftResult  success = cufftPlanMany(
    &plan, rank, n,
    inembed, istride, idist,
    onembed, ostride, odist,
    cufft_type_map::flag, howmany);
  if (success != CUFFT_SUCCESS) {
    throw "Failed to create cuFFT plan";
  }
  this->is_setup = true;
}


template<typename dtype>
void PolyphaseFilterBankEngineCUDA<dtype>::operator() (
  dtype* _input,  dtype* _filter_coeff, dtype* _output
)
{
  #if VERBOSE
    std::cerr << "pfb::PolyphaseFilterBankEngineCUDA::operator()" << std::endl;
  #endif

  apply_filter(
    _input, _filter_coeff, _output
  );
  cufftResult success = cufft_type_map::execute(plan, _output, _output, CUFFT_FORWARD);
  if (success != CUFFT_SUCCESS) {
    throw "Failed to execute cuFFT plan";
  }
}


template<typename dtype>
void PolyphaseFilterBankEngineCUDA<dtype>::print_array (
  const float* arr, unsigned size
)
{
  #if VERBOSE
    std::cerr << "pfb::PolyphaseFilterBankEngineCUDA::print_array()" << std::endl;
  #endif

  k_print_array<float><<<1, 1>>>(arr, size);

}

template<typename dtype>
void PolyphaseFilterBankEngineCUDA<dtype>::apply_filter(
  dtype* input,
  dtype* filter_coeff,
  dtype* output
)
{
  #if VERBOSE
    std::cerr << "pfb::PolyphaseFilterBankEngineCUDA::apply_filter" << std::endl;
  #endif

  unsigned taps_per_sample = this->filter_taps_padded / this->channels;

  if (taps_per_sample <= 32) {

    int nthreads = 1024;
    int nblocks_x = 1;

    if (this->nblocks < nthreads) {
      nthreads = this->nblocks;
    }

    if (nthreads >= 1024) {
      nblocks_x = this->nblocks / nthreads;
    }

    dim3 grid (nblocks_x, this->channels, 1);
    dim3 block (nthreads, 1, 1);

    k_apply_filter<dtype><<<grid, block>>>(
      input,
      output,
      filter_coeff,
      this->filter_taps_padded,
      this->step,
      this->channels,
      this->nblocks,
      taps_per_sample
    );
  } else {
    int nthreads = taps_per_sample;
    if (nthreads > 1024) {
      nthreads = 1024;
    }

    dim3 grid (this->nblocks, this->channels, 1);
    dim3 block (nthreads, 1, 1);

    size_t shared_bytes = nthreads / 32;
    if (shared_bytes == 0) {
      shared_bytes++;
    }
    shared_bytes *= sizeof(dtype);

    k_apply_filter_reduce_sum<dtype><<<grid, block, shared_bytes>>>(
      input,
      output,
      filter_coeff,
      this->filter_taps_padded,
      this->step,
      this->channels,
      this->nblocks,
      taps_per_sample
    );
  }
}

template class PolyphaseFilterBankEngineCUDA<cufftComplex>;
template class PolyphaseFilterBankEngineCUDA<cufftDoubleComplex>;
}
