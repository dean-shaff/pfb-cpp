import os

from distutils.core import Extension

from distutils.command.build import build


class CustomBuild(build):
    sub_commands = [
        ('build_ext', build.has_ext_modules),
        ('build_py', build.has_pure_modules),
        ('build_clib', build.has_c_libraries),
        ('build_scripts', build.has_scripts),
    ]


base_dir = os.path.dirname(os.path.abspath(__file__))
backends_dir = os.path.join(base_dir, "pfb", "_backends")

backends_include_dirs = [
    os.path.join(backends_dir, "include"),
    os.path.join(backends_dir, "src")
]

backends_libraries = ["fftw3f", "fftw3"]

swig_opts = ["-py3", "-c++"]
swig_opts.extend([f"-I{p}" for p in backends_include_dirs])


swig_extension = Extension(
    "pfb._backends._pfb_backend", [
        os.path.join(backends_dir, "src", "pfb_backend.i"),
        os.path.join(backends_dir, "src", "PolyphaseFilterBankEngineCPU.cpp"),
        os.path.join(backends_dir, "src", "util.cpp")
    ],
    swig_opts=swig_opts,
    include_dirs=backends_include_dirs,
    libraries=backends_libraries
)


extensions = [
    swig_extension
]


def build(setup_kwargs):
    """
    This function is mandatory in order to build the extensions.
    """
    setup_kwargs.update(
        {"ext_modules": extensions}
    )
