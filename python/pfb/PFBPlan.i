%module PFBPlan

%{
#define SWIG_FILE_WITH_INIT
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include <cstring>

#include <cuda_runtime.h>
#include <cufft.h>
#include "numpy/noprefix.h"
#include "numpy/arrayobject.h"

#include "pfb/util/util.hpp"
#include "pfb/PolyphaseFilterBankPlan.hpp"
#include "pfb/PolyphaseFilterBankEngineCUDA.hpp"

namespace pfb {

  template<int T>
  struct np2std;

  template<>
  struct np2std<NPY_COMPLEX64> {
    typedef std::complex<float> type;
  };

  template<>
  struct np2std<NPY_COMPLEX128> {
    typedef std::complex<double> type;
  };

  template<int T>
  struct np2cufft;

  template<>
  struct np2cufft<NPY_COMPLEX64> {
    typedef cufftComplex type;
  };

  template<>
  struct np2cufft<NPY_COMPLEX128> {
    typedef cufftDoubleComplex type;
  };

}

%}


%init %{
import_array();
%}

%typemap(out) pfb::util::four_tuple {
    PyTupleObject *res = (PyTupleObject *)PyTuple_New(4);
    PyTuple_SetItem((PyObject *)res, 0, PyLong_FromUnsignedLong(std::get<0>($1)));
    PyTuple_SetItem((PyObject *)res, 1, PyLong_FromUnsignedLong(std::get<1>($1)));
    PyTuple_SetItem((PyObject *)res, 2, PyLong_FromUnsignedLong(std::get<2>($1)));
    PyTuple_SetItem((PyObject *)res, 3, PyLong_FromUnsignedLong(std::get<3>($1)));
    $result = (PyObject *)res;
}

%inline %{
namespace pfb {
namespace util {

  PyArrayObject* numpy_get_arr_from_obj (PyObject* obj) {
    const int dtype_obj = PyArray_ObjectType(obj, NPY_FLOAT);
    return (PyArrayObject*) PyArray_FROM_OTF(obj, dtype_obj, NPY_ARRAY_IN_ARRAY);
  }

  long pycuda_get_ptr (PyObject* obj) {
    return PyLong_AsLongLong(
      PyObject_GetAttrString(obj, "ptr")
    );
  }

  long long pycuda_get_dtype_num (PyObject* obj) {
    return PyLong_AsLong(
        PyObject_GetAttrString(
            PyObject_GetAttrString(obj, "dtype"),
            "num"));
  }

  long long pycuda_get_size (PyObject* obj) {
    return (unsigned) PyLong_AsLong(
        PyTuple_GetItem(
          PyObject_GetAttrString(obj, "shape"), 0));
  }
}
}
%}

%include "pfb/PolyphaseFilterBankPlan.hpp"
%include "pfb/util/util.hpp"


%template(PFBPlanCPU_float) pfb::PolyphaseFilterBankPlan<std::complex<float>>;
%template(PFBPlanCPU_double) pfb::PolyphaseFilterBankPlan<std::complex<double>>;
%template(PFBPlanCUDA_float) pfb::PolyphaseFilterBankPlan<
  cufftComplex, pfb::PolyphaseFilterBankEngineCUDA<cufftComplex>>;
%template(PFBPlanCUDA_double) pfb::PolyphaseFilterBankPlan<
  cufftDoubleComplex, pfb::PolyphaseFilterBankEngineCUDA<cufftDoubleComplex>>;

%define PolyphaseFilterBankPlanCPU__call__Extend(PlanType, Type)
%extend PlanType<std::complex<Type>> {

  PyObject* __call__ (PyObject* in, PyObject* filter, PyObject* ret)
  {

    PyArrayObject* ret_arr = pfb::util::numpy_get_arr_from_obj(ret);
    PyArrayObject* in_arr = pfb::util::numpy_get_arr_from_obj(in);
    PyArrayObject* filter_arr = pfb::util::numpy_get_arr_from_obj(filter);

    $self->operator() (
      (std::complex<Type>*) PyArray_BYTES(in_arr),
      (std::complex<Type>*) PyArray_BYTES(filter_arr),
      (std::complex<Type>*) PyArray_BYTES(ret_arr)
    );
    Py_INCREF(Py_None);
    return Py_None;
  }
}
%enddef
PolyphaseFilterBankPlanCPU__call__Extend(pfb::PolyphaseFilterBankPlan, float)
PolyphaseFilterBankPlanCPU__call__Extend(pfb::PolyphaseFilterBankPlan, double)

%define PolyphaseFilterBankPlanCUDA__call__Extend(PlanType, Type)
%extend pfb::PolyphaseFilterBankPlan<
  Type, pfb::PolyphaseFilterBankEngineCUDA<Type>> {

  PyObject* __call__ (PyObject* in, PyObject* filter, PyObject* ret)
  {

    long long in_ptr = pfb::util::pycuda_get_ptr(in);
    long long filter_ptr = pfb::util::pycuda_get_ptr(filter);
    long long ret_ptr = pfb::util::pycuda_get_ptr(ret);

    Type* d_in = reinterpret_cast<Type*>(in_ptr);
    Type* d_filter = reinterpret_cast<Type*>(filter_ptr);
    Type* d_ret = reinterpret_cast<Type*>(ret_ptr);

    $self->operator() (
      d_in, d_filter, d_ret);
    pfb::util::check_error("polyphase_filterbank 64");
    Py_INCREF(Py_None);
    return Py_None;
  }
}
%enddef
PolyphaseFilterBankPlanCUDA__call__Extend(pfb::PolyphaseFilterBankPlan, cufftComplex)
PolyphaseFilterBankPlanCUDA__call__Extend(pfb::PolyphaseFilterBankPlan, cufftDoubleComplex)

// %inline %{
//
// namespace pfb {
//
//   PyObject* polyphase_analysis_in_place_cuda(
//     PyObject* in,
//     PyObject* filter,
//     PyObject* ret,
//     int channels,
//     const pfb::util::rational& os_factor
//   )
//   {
//
//     long long in_ptr = pfb::util::pycuda_get_ptr(in);
//     long in_dtype_num = pfb::util::pycuda_get_dtype_num(in);
//     unsigned in_size = pfb::util::pycuda_get_size(in);
//
//     long long filter_ptr = pfb::util::pycuda_get_ptr(filter);
//     long filter_dtype_num = pfb::util::pycuda_get_dtype_num(filter);
//     unsigned filter_size = pfb::util::pycuda_get_size(filter);
//
//     long long ret_ptr = pfb::util::pycuda_get_ptr(ret);
//     long ret_dtype_num = pfb::util::pycuda_get_dtype_num(ret);
//
//     if (in_dtype_num == NPY_COMPLEX64) {
//       typedef typename pfb::np2cufft<NPY_COMPLEX64>::type z_type;
//       z_type* d_in = reinterpret_cast<z_type*>(in_ptr);
//       z_type* d_filter = reinterpret_cast<z_type*>(filter_ptr);
//       z_type* d_ret = reinterpret_cast<z_type*>(ret_ptr);
//
//       pfb::PolyphaseFilterBankPlan<z_type, pfb::PolyphaseFilterBankEngineCUDA<z_type>> plan(
//         channels, os_factor, in_size, filter_size);
//
//       plan(
//         d_in, d_filter, d_ret);
//       pfb::util::check_error("polyphase_filterbank 64");
//
//     } else if (in_dtype_num == NPY_COMPLEX128) {
//       typedef typename pfb::np2cufft<NPY_COMPLEX128>::type z_type;
//       z_type* d_in = reinterpret_cast<z_type*>(in_ptr);
//       z_type* d_filter = reinterpret_cast<z_type*>(filter_ptr);
//       z_type* d_ret = reinterpret_cast<z_type*>(ret_ptr);
//
//       pfb::PolyphaseFilterBankPlan<z_type, pfb::PolyphaseFilterBankEngineCUDA<z_type>> plan(
//         channels, os_factor, in_size, filter_size);
//
//       plan(
//         d_in, d_filter, d_ret);
//       pfb::util::check_error("polyphase_filterbank 128");
//
//
//     } else {
//       PyErr_SetString(PyExc_ValueError, "Inputs need to be of NPY_COMPLEX64 or NPY_COMPLEX128 type");
//       return NULL;
//     }
//
//
//     Py_INCREF(Py_None);
//     return Py_None;
//
//   }
//
//
//   PyObject* polyphase_analysis_in_place_cpu(
//     PyObject* in,
//     PyObject* filter,
//     PyObject* ret,
//     int channels,
//     const pfb::util::rational& os_factor
//   )
//   {
//     PyArrayObject* ret_arr=NULL;
//     PyArrayObject* in_arr=NULL;
//     PyArrayObject* filter_arr=NULL;
//
//     const int dtype_in = PyArray_ObjectType(in, NPY_FLOAT);
//     const int dtype_filter = PyArray_ObjectType(filter, NPY_FLOAT);
//
//     in_arr = (PyArrayObject*) PyArray_FROM_OTF(in, dtype_in, NPY_ARRAY_IN_ARRAY);
//     filter_arr = (PyArrayObject*) PyArray_FROM_OTF(filter, dtype_filter, NPY_ARRAY_IN_ARRAY);
//     ret_arr = (PyArrayObject*) PyArray_FROM_OTF(ret, dtype_in, NPY_ARRAY_IN_ARRAY);
//
//     const int size_in = PyArray_DIMS(in_arr)[0];
//     const int size_filter = PyArray_DIMS(filter_arr)[0];
//
//     if (dtype_in == NPY_COMPLEX64) {
//       typedef typename pfb::np2std<NPY_COMPLEX64>::type z_type_8;
//
//       pfb::PolyphaseFilterBankPlan<z_type_8> plan(
//         channels, os_factor, size_in, size_filter
//       );
//
//       plan(
//         (z_type_8*) PyArray_BYTES(in_arr),
//         (z_type_8*) PyArray_BYTES(filter_arr),
//         (z_type_8*) PyArray_BYTES(ret_arr)
//       );
//
//     } else if (dtype_in == NPY_COMPLEX128) {
//       typedef typename pfb::np2std<NPY_COMPLEX128>::type z_type_16;
//
//       pfb::PolyphaseFilterBankPlan<z_type_16> plan(
//         channels, os_factor, size_in, size_filter
//       );
//       plan(
//         (z_type_16*) PyArray_BYTES(in_arr),
//         (z_type_16*) PyArray_BYTES(filter_arr),
//         (z_type_16*) PyArray_BYTES(ret_arr)
//       );
//
//     } else {
//       PyErr_SetString(PyExc_ValueError, "Inputs need to be of NPY_COMPLEX64 or NPY_COMPLEX128 type");
//       return NULL;
//     }
//
//     Py_INCREF(Py_None);
//     return Py_None;
//   }
// }
// %}

%pythoncode %{
import typing

import numpy as np
import pycuda.gpuarray as gpuarray

array_type = typing.Union[np.array, gpuarray.GPUArray]

class PFBPlan:

  def __init__(
      self,
      channels: int,
      os_factor: rational,
      in_size: int,
      filter_taps: int,
      dtype: type = np.complex64,
      gpu: bool = False
  ):
    self._channels = channels
    self._os_factor = os_factor
    self._in_size = in_size
    self._filter_taps = filter_taps
    self._dtype = dtype
    self._gpu = gpu

    self._plan_cls = self._get_plan_cls()
    self._plan = self._plan_cls(
        self._channels, self._os_factor, self._in_size, self._filter_taps
    )

  def _get_plan_cls(self):
    """
    Given a numpy dtype and a gpu flag, get the right wrapped C++ plan class.
    """
    if self._dtype == np.complex64:
      if self._gpu:
        return PFBPlanCUDA_float
      else:
        return PFBPlanCPU_float
    elif self._dtype == np.complex128:
      if self._gpu:
        return PFBPlanCUDA_double
      else:
        return PFBPlanCPU_double

  def _get_plan_obj(self):
    return self._plan_cls(
        self._channels, self._os_factor, self._in_size, self._filter_taps
    )

  def __call__(self,
               in_arr: array_type,
               filter_arr: array_type,
               out_arr: array_type = None):
    arr_cls = gpuarray.zeros if self.gpu else np.zeros
    if out_arr is None:
      out_arr = arr_cls((self.get_out_size(), ), dtype=in_arr.dtype)
    self._plan(in_arr, filter_arr, out_arr)

    return out_arr

  def __getattr__(self, name):
      if hasattr(self._plan, name):
          return getattr(self._plan, name)

  @property
  def channels(self):
    return self._channels

  @channels.setter
  def channels(self, channels):
    self._channels = channels
    self._plan = self._get_plan_obj()

  @property
  def os_factor(self):
    return self._os_factor

  @os_factor.setter
  def os_factor(self, os_factor):
    self._os_factor = os_factor
    self._plan = self._get_plan_obj()

  @property
  def in_size(self):
    return self._in_size

  @in_size.setter
  def in_size(self, in_size):
    self._in_size = in_size
    self._plan = self._get_plan_obj()

  @property
  def filter_taps(self):
    return self._filter_taps

  @filter_taps.setter
  def filter_taps(self, filter_taps):
    self._filter_taps = filter_taps
    self._plan = self._get_plan_obj()

  @property
  def dtype(self):
    return self._dtype

  @dtype.setter
  def dtype(self, dtype):
    self._dtype = dtype
    self._plan_cls = self._get_plan_cls()
    self._plan = self._get_plan_obj()

  @property
  def gpu(self):
    return self._gpu

  @gpu.setter
  def gpu(self, gpu):
    self._gpu = gpu
    self._plan_cls = self._get_plan_cls()
    self._plan = self._get_plan_obj()


# def polyphase_analysis(in_arr, filter_arr, channels, os_factor, gpu=False):
#
#   if not gpu:
#     res = calc_out_size(
#       channels,
#       os_factor,
#       in_arr.shape[0],
#       filter_arr.shape[0]
#     )
#
#     out_arr = np.zeros(res[-1], dtype=in_arr.dtype)
#     filter_padded = np.zeros(res[0], dtype=filter_arr.dtype)
#     filter_padded[:filter_arr.shape[0]] = filter_arr
#
#     polyphase_analysis_in_place_cpu(
#       in_arr, filter_padded, out_arr, channels, os_factor)
#   else:
#     res = calc_out_size(
#       channels,
#       os_factor,
#       in_arr.shape[0],
#       filter_arr.shape[0]
#     )
#     out_arr = gpuarray.zeros(res[-1], dtype=in_arr.dtype)
#     filter_padded = gpuarray.zeros(res[0], dtype=filter_arr.dtype)
#     filter_padded[:filter_arr.shape[0]] = filter_arr
#
#     polyphase_analysis_in_place_cuda(
#       in_arr, filter_padded, out_arr, channels, os_factor)
#
#
#   return out_arr
%}
