#!/usr/bin/env python

from distutils.core import setup

from pfb import __version__

from build import extensions, CustomBuild

setup(
    name="pfb",
    version=__version__,
    description="PFB channelization",
    author="Dean Shaff",
    author_email="dean.shaff@gmail.com",
    url="https://github.com/dean-shaff/pfb",
    packages=[
        "pfb"
    ],
    py_modules=["pfb._backends.pfb_backend"],
    cmdclass={'build': CustomBuild},
    ext_modules=extensions,
)
