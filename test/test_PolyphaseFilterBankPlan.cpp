#include <vector>
#include <complex>

#include "catch.hpp"

#include "pfb/PolyphaseFilterBankPlan.hpp"

TEMPLATE_TEST_CASE (
  "can create pfb::PolyphaseFilterBankPlan objects",
  "[PolyphaseFilterBankPlan][unit]",
  std::complex<float>, std::complex<double>
)
{
  pfb::PolyphaseFilterBankPlan<TestType> plan(
    8, pfb::util::rational(8, 7), 1200, 161);

  REQUIRE(plan.get_channels() == 8);
  REQUIRE(plan.get_os_factor() == pfb::util::rational(8, 7));
  REQUIRE(plan.get_in_size() == 1200);
  REQUIRE(plan.get_filter_taps() == 161);
  REQUIRE(plan.get_filter_taps_padded() == 168);
  REQUIRE(plan.get_out_size() == 1176);
  REQUIRE(plan.get_step() == 7);
  REQUIRE(plan.get_nblocks() == 147);
}
