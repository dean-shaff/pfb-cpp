#include <vector>
#include <complex>
#include <iostream>

#include "catch.hpp"

#include "pfb/util/util.hpp"
#if HAVE_CUDA
#include "pfb/util/cuda_container.hpp"
#endif

#include "util.hpp"


TEST_CASE ("can create pfb::util::rational objects", "[unit][util]")
{
  pfb::util::rational r(4, 3);
  REQUIRE(r.nu == 4);
  REQUIRE(r.de == 3);
}

TEST_CASE ("can compare pfb::util::rational objects", "[unit][util]")
{
  pfb::util::rational r(4, 3);
  REQUIRE(r == pfb::util::rational(4, 3));
}



TEST_CASE ("calc_out_size produces correct result", "[unit][util]")
{
  unsigned expected_filter_taps_padded = 128;
  unsigned expected_step = 6;
  unsigned expected_nblocks = 1645;
  unsigned expected_out_size = 13160;

  unsigned channels = 8;
  pfb::util::rational r(4, 3);
  unsigned in_size = 10002;
  unsigned filter_taps = 121;

  pfb::util::four_tuple res = pfb::util::calc_out_size(channels, r, in_size, filter_taps);

  REQUIRE(std::get<0>(res) == expected_filter_taps_padded);
  REQUIRE(std::get<1>(res) == expected_step);
  REQUIRE(std::get<2>(res) == expected_nblocks);
  REQUIRE(std::get<3>(res) == expected_out_size);
}


TEST_CASE ("calc_filter_taps_padded produces correct result", "[unit][util]")
{
  unsigned expected = 128;
  unsigned channels = 8;
  unsigned filter_taps = 121;

  REQUIRE(pfb::util::calc_filter_taps_padded(channels, filter_taps) == expected);
}


#if HAVE_CUDA
TEMPLATE_TEST_CASE (
  "CUDAContainer handles memory on GPU",
  "[unit][util][cuda][CUDAContainer]",
  cufftComplex, cufftDoubleComplex
)
{

  unsigned size = 100;
  typedef typename pfb::util::cufft2std<TestType>::type std_type;
  typedef typename pfb::util::complex2float<std_type>::type float_type;


  SECTION ("Can allocate data on device")
  {
    pfb::util::CUDAContainer<TestType> container(size);
    REQUIRE(container.size() == size);
  }

  SECTION ("Can allocate and copy data in constructor with vector")
  {
    std::vector<std_type> vec(size);
    pfb::util::CUDAContainer<TestType> container (vec);
    REQUIRE(vec.size() == container.size());
  }

  SECTION ("Can allocate and copy data in constructor with pointer")
  {
    std::vector<std_type> vec(size);
    pfb::util::CUDAContainer<TestType> container (vec.data(), vec.size());
    REQUIRE(vec.size() == container.size());
  }

  SECTION ("Can copy data from vector to device")
  {
    pfb::util::CUDAContainer<TestType> container(size);
    std::vector<std_type> vec(size + 1);
    container.h2d(vec);
    REQUIRE(container.size() == vec.size());
  }

  SECTION ("Can copy data from pointer to device")
  {
    pfb::util::CUDAContainer<TestType> container(size);
    std::vector<std_type> vec(size + 1);
    container.h2d(vec.data(), vec.size());
    REQUIRE(container.size() == vec.size());
  }

  SECTION ("Can copy data from device to vector")
  {
    pfb::util::CUDAContainer<TestType> container(size);
    std::vector<std_type> vec;
    container.d2h(vec);
    REQUIRE(vec.size() == container.size());
  }

  SECTION ("Can copy data from device to pointer")
  {
    pfb::util::CUDAContainer<TestType> container(size);
    std::vector<std_type> vec(size);
    container.d2h(vec.data());
    REQUIRE(vec.size() == container.size());
  }

  SECTION ("Copies correct data to device and back")
  {
    std::vector<std_type> expected(size);
    std::vector<std_type> test_vec;
    auto rand_gen = util::random<float_type>();
    for (unsigned idx=0; idx<size; idx++)
    {
      expected[idx] = std_type(rand_gen(), rand_gen());
    }
    pfb::util::CUDAContainer<TestType> container(expected);

    container.d2h(test_vec);

    bool allclose = true;
    unsigned nclose = 0;
    for (unsigned idx=0; idx<size; idx++)
    {
      if (expected[idx] != test_vec[idx]) {
        allclose = false;
      } else {
        nclose++;
      }
    }

    std::cerr << "test_pfb_util: "
      << nclose << "/" << size
      << " (" << 100 * nclose / size
      << "%) equal" << std::endl;
    REQUIRE(allclose == true);

  }


}



#endif
