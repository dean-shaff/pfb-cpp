#ifndef __util_test_hpp
#define __util_test_hpp

#include <complex>
#include <chrono>
#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <time.h>
#include <functional>
#include <exception>

#include "toml.h"

namespace util {

  template<typename T>
  struct strip_complex;

  template<typename T>
  struct strip_complex<std::complex<T>> {
    typedef T type;
  };

  template<typename T>
  bool isclose (T a, T b, double atol, double rtol=1e-8);

  template<typename T>
  bool load_binary_data (std::string file_path, std::vector<T>& test_data);

  template<typename T>
  void write_binary_data (std::string file_path, std::vector<T>& data);

  template<typename T>
  void write_binary_data (std::string file_path, T* buffer, int len);

  inline std::string get_env_var (
    const std::string& name,
    const std::string& default_val
  )
  {
    const char* env_var = std::getenv(name.c_str());
    if (env_var) {
      return std::string(env_var);
    } else {
      return std::string(default_val);
    }
  }

  inline std::chrono::time_point<std::chrono::high_resolution_clock> now () {
    return std::chrono::high_resolution_clock::now();
  }

  template<typename T>
  std::function<T(void)> random ();

  class TOMLConfig {
  public:

    TOMLConfig () {
      default_file_path = util::get_env_var(
        "TEST_DIR", "./../test") + "/" + "test.config.toml";
    }

    void load_config (const std::string& _file_path = "") {
      std::string file_path = _file_path;
      if (file_path == "") {
        file_path = default_file_path;
      }

      std::ifstream ifs(file_path);
      if (! ifs.good()) {

        throw std::runtime_error(
          "test::util::TOMLConfig::load_config: file_path is either nonexistent or locked");
      }
      toml::ParseResult pr = toml::parse(ifs);
      ifs.close();

      if (! pr.valid())
      {
        throw std::runtime_error(
          "test::util::TOMLConfig::load_config: invalid TOML file");
      }
      config = pr.value;
    }

    template<class T>
    T get (const std::string& name)
    {
      if (! config_loaded) {
        load_config();
      }

      return (T) config.get<T>(name);
    }



  private:
    toml::Value config;
    bool config_loaded;

    std::string default_file_path;

  };


}

template<typename T>
bool util::isclose (T a, T b, double atol, double rtol)
{
  return abs(a - b) <= (atol + rtol * abs(b));
}


template<typename T>
bool util::load_binary_data (std::string file_path, std::vector<T>& test_data)
{
  std::streampos size;

  std::ifstream file (file_path, std::ios::in|std::ios::binary|std::ios::ate);
  if (file.is_open())
  {
    file.seekg(0, std::ios::end);
    size = file.tellg();
    file.seekg(0, std::ios::beg);

    // read the data:
    std::vector<char> file_bytes(size);
    file.read(&file_bytes[0], size);
    file.close();

    int T_size = (size / sizeof(T));
    // std::cerr << "T_size=" << T_size << std::endl;

    const T* data = reinterpret_cast<const T*>(file_bytes.data());
    test_data.assign(data, data + T_size);
    return true;
  } else {
    std::cerr << "file " << file_path << " is not open" << std::endl;
    return false;
  }
}

template<typename T>
void util::write_binary_data (std::string file_path, std::vector<T> buffer)
{
  util::write_binary_data(file_path, buffer.data(), buffer.size());
}

template<typename T>
void util::write_binary_data (std::string file_path, T* buffer, int len)
{
  std::ofstream file(file_path, std::ios::out | std::ios::binary);

  file.write(
    reinterpret_cast<const char*>(buffer),
    len*sizeof(T)
  );
  file.close();
}


template<typename T>
std::function<T(void)> util::random ()
{
  srand (time(NULL));
  return [] () { return (T) rand() / RAND_MAX; };
}

#endif
