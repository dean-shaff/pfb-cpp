#include <string>

#include "util.hpp"

std::chrono::time_point<std::chrono::high_resolution_clock> util::now () {
  return std::chrono::high_resolution_clock::now();
}


std::string util::get_test_data_dir () {
  const char* env_data_dir = std::getenv("TEST_DATA_DIR");
  if (env_data_dir) {
    return std::string(env_data_dir);
  } else {
    return ".";
  }
}
