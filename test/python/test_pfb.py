import sys
import unittest

sys.path.insert(0, "/fred/oz002/users/dshaff/software/sources/pfb-cpp/build/python")

import pycuda.autoinit
import pycuda.gpuarray as gpuarray
import numpy as np
import pfb


class TestPFB(unittest.TestCase):

    os_factor = pfb.rational(4, 3)

    in_size = 1200
    filter_size = 120
    nchan = 8

    def test_polyphase_analysis_gpu(self):

        for dtype in [np.float32, np.float64]:
            in_arr = (np.ones(self.in_size, dtype=dtype) +
                      1j*np.ones(self.in_size, dtype=dtype))
            filter_arr = (2*np.ones(self.filter_size, dtype=dtype) +
                          1j*np.zeros(self.filter_size, dtype=dtype))

            pfb.polyphase_analysis(
                gpuarray.to_gpu(in_arr),
                gpuarray.to_gpu(filter_arr),
                self.nchan,
                self.os_factor,
                gpu=True)

    def test_polyphase_analysis(self):

        for dtype in [np.float32, np.float64]:
            in_arr = (np.ones(self.in_size, dtype=dtype) +
                      1j*np.ones(self.in_size, dtype=dtype))
            filter_arr = (2*np.ones(self.filter_size, dtype=dtype) +
                          1j*np.zeros(self.filter_size, dtype=dtype))

            pfb.polyphase_analysis(
                in_arr, filter_arr, self.nchan, self.os_factor, gpu=False)

        dtype = np.int32

        with self.assertRaises(ValueError):
            in_arr = np.ones(self.in_size, dtype=np.int32)
            filter_arr = 2*np.ones(self.filter_size, dtype=np.int32)
            pfb.polyphase_analysis(in_arr, filter_arr, self.nchan, self.os_factor)


class TestPFBPlan(unittest.TestCase):

    os_factor = pfb.rational(4, 3)

    in_size = 1200
    filter_size = 120
    nchan = 8

    def test_init(self):

        for dtype in [np.complex64, np.complex128]:
            for gpu in [True, False]:
                pfb.PFBPlan(
                    self.nchan,
                    self.os_factor,
                    self.in_size,
                    self.filter_size,
                    dtype, gpu)

    def test_getattr(self):
        plan = pfb.PFBPlan(
            self.nchan,
            self.os_factor,
            self.in_size,
            self.filter_size,
            np.complex64, False)

        res = plan.get_out_size()
        self.assertTrue(isinstance(res, int))

    def test_call(self):
        for dtype in [np.complex64, np.complex128]:
            for gpu in [True, False]:
                plan = pfb.PFBPlan(
                    self.nchan,
                    self.os_factor,
                    self.in_size,
                    self.filter_size,
                    dtype, gpu)
                in_arr = np.ones((self.in_size, ), dtype=dtype)
                filter_arr = 2*np.ones((self.filter_size, ), dtype=dtype)
                if gpu:
                    in_arr = gpuarray.to_gpu(in_arr)
                    filter_arr = gpuarray.to_gpu(filter_arr)
                ret_arr = plan(in_arr, filter_arr)
                self.assertTrue(ret_arr.shape[0] == plan.get_out_size())


if __name__ == "__main__":
    unittest.main()
