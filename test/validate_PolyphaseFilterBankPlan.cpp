// Ensure that this PFB implementation matches a reference version to within
// numerical accuracy. The reference implementation is contained in some binary
// data files.
#include <tuple>
#include <string>
#include <vector>
#include <complex>
#include <iostream>

#include "catch.hpp"

#include "pfb/PolyphaseFilterBankPlan.hpp"

#include "util.hpp"

static util::TOMLConfig config;

TEST_CASE (
  "PFB produces numerically correct output",
  "[validate][PolyphaseFilterBankPlan]"
)
{

  std::string test_data_dir = util::get_env_var(
    "TEST_DIR", "./../test") + "/test_data";

  struct file_info {
    bool padded;
    std::string input_file_name;
    std::string filter_file_name;
    std::string output_file_name;
    unsigned taps;
    unsigned blocks;
    unsigned channels;
    pfb::util::rational os_factor;
  };

  config.load_config();

  const double thresh = config.get<double>("thresh");

  const toml::Array toml_file_config = config.get<toml::Array>(
    "validate_PolyphaseFilterBankPlan");

  std::vector<file_info> file_config (toml_file_config.size());

  for (unsigned idx=0; idx<file_config.size(); idx++) {
    file_config[idx].padded = toml_file_config[idx].get<bool>("padded");
    file_config[idx].input_file_name = toml_file_config[idx].get<std::string>("input_file_name");
    file_config[idx].filter_file_name = toml_file_config[idx].get<std::string>("filter_file_name");
    file_config[idx].output_file_name = toml_file_config[idx].get<std::string>("output_file_name");
    file_config[idx].taps = (unsigned) toml_file_config[idx].get<int>("taps");
    file_config[idx].blocks = (unsigned) toml_file_config[idx].get<int>("blocks");
    file_config[idx].channels = (unsigned) toml_file_config[idx].get<int>("channels");
    file_config[idx].os_factor = pfb::util::rational(
      (unsigned) toml_file_config[idx].get<int>("os_factor_nu"),
      (unsigned) toml_file_config[idx].get<int>("os_factor_de")
    );
  }


  for (unsigned idx=0; idx<file_config.size(); idx++) {
    std::vector<std::complex<double>> input_vector;
    std::vector<std::complex<double>> fir_filter_coeff;
    std::vector<std::complex<double>> output_expected;
    std::vector<std::complex<double>> output_vector;

    bool success;
    unsigned channels = file_config[idx].channels;
    unsigned blocks = file_config[idx].blocks;

    success = util::load_binary_data<std::complex<double>> (
      test_data_dir + "/" + file_config[idx].input_file_name, input_vector);
    success = util::load_binary_data<std::complex<double>> (
      test_data_dir + "/" + file_config[idx].filter_file_name, fir_filter_coeff);
    success = util::load_binary_data<std::complex<double>> (
      test_data_dir + "/" + file_config[idx].output_file_name, output_expected);

    // std::cerr << "input_vector.size()=" << input_vector.size() << std::endl;
    // std::cerr << "fir_filter_coeff.size()=" << fir_filter_coeff.size() << std::endl;
    // std::cerr << "output_expected.size()=" << output_expected.size() << std::endl;

    if (! success) {
      throw "Can't find data files!";
    }

    pfb::PolyphaseFilterBankPlan<std::complex<double>> plan(
      file_config[idx].channels, file_config[idx].os_factor,
      input_vector.size(), fir_filter_coeff.size()
    );
    fir_filter_coeff.resize(plan.get_filter_taps_padded());
    output_vector.resize(plan.get_out_size());

    plan(input_vector.data(), fir_filter_coeff.data(), output_vector.data());

    CHECK(blocks == plan.get_nblocks());

    bool allclose = true;
    unsigned nclose = 0;

    unsigned index;
    for (unsigned iblock=0; iblock<blocks; iblock++) {
      for (unsigned ichan=0; ichan<channels; ichan++) {
        index = ichan + channels*iblock;
        if (! util::isclose(output_vector[index], output_expected[index], thresh, thresh)) {
          std::cerr << "iblock=" << iblock << ", ichan=" << ichan << std::endl;
          allclose = false;
        } else {
          nclose++;
        }
      }
    }

    std::cerr << "validate_PolyphaseFilterBankPlan: "
      << nclose << "/" << output_vector.size()
      << " (" << 100 * nclose / output_vector.size()
      << "%) equal" << std::endl;
    CHECK(allclose == true);
  }


}
