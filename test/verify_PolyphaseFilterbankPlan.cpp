// Verify that the CUDA and CPU bound implementations produce the same output.
#include <iostream>

#include "catch.hpp"

#include "pfb/PolyphaseFilterBankPlan.hpp"
#include "pfb/PolyphaseFilterBankEngineCPU.hpp"
#include "pfb/PolyphaseFilterBankEngineCUDA.hpp"

#include "util.hpp"

const double abs_tol = 1e-4;
const double rel_tol = 1e-3;

TEMPLATE_TEST_CASE (
  "PolyphaseFilterBankPlan engines produces same results",
  "[verify][PolyphaseFilterBankPlan][cuda]",
  double)
{
  typedef std::complex<TestType> TestTypeComplex;
  typedef typename pfb::std2cufft<TestTypeComplex>::type cufft_type;
  size_t sz = sizeof(cufft_type);

  pfb::util::rational os_factor(4, 3);

  unsigned channels = 256;
  unsigned in_size = 120000;
  unsigned ntaps = 1280;

  // unsigned channels = 4;
  // unsigned in_size = 60;
  // unsigned ntaps = 12;

  std::vector<TestTypeComplex> in(in_size);
  std::vector<TestTypeComplex> filter(ntaps, TestTypeComplex(2.0, 0.0));
  std::vector<TestTypeComplex> out_cpu;
  std::vector<TestTypeComplex> out_gpu;

  cufft_type* in_d;
  cufft_type* filter_d;
  cufft_type* out_d;

  auto rando_gen = util::random<TestType>();
  for (unsigned i=0; i<in_size; i++) {
    in[i] = TestTypeComplex(rando_gen(), rando_gen());
  }

  pfb::PolyphaseFilterBankPlan<TestTypeComplex, pfb::PolyphaseFilterBankEngineCPU<TestTypeComplex>> plan_cpu(
    channels, os_factor, in_size, ntaps);
  pfb::PolyphaseFilterBankPlan<cufft_type, pfb::PolyphaseFilterBankEngineCUDA<cufft_type>> plan_cuda(
    channels, os_factor, in_size, ntaps);

  unsigned out_size = plan_cpu.get_out_size();
  unsigned filter_taps_padded = plan_cpu.get_filter_taps_padded();

  out_cpu.resize(out_size);
  out_gpu.resize(out_size);
  filter.resize(filter_taps_padded);

  cudaMalloc((void **) &in_d, in_size*sz);
  cudaMalloc((void **) &out_d, out_size*sz);
  cudaMalloc((void **) &filter_d, filter_taps_padded*sz);

  cudaMemcpy(
    in_d,
    (cufft_type*) in.data(),
    in_size*sz,
    cudaMemcpyHostToDevice);

  cudaMemcpy(
    filter_d,
    (cufft_type*) filter.data(),
    filter_taps_padded*sz,
    cudaMemcpyHostToDevice);

  auto now = util::now();
  plan_cpu(in.data(), filter.data(), out_cpu.data());
  std::chrono::duration<double, std::milli> delta_cpu = util::now() - now;

  now = util::now();
  plan_cuda(in_d, filter_d, out_d);
  pfb::util::check_error("PolyphaseFilterBankPlan");
  std::chrono::duration<double, std::milli> delta_gpu = util::now() - now;

  cudaMemcpy(
    (cufft_type*) out_gpu.data(),
    out_d,
    out_size*sz,
    cudaMemcpyDeviceToHost);

  cudaFree(in_d);
  cudaFree(out_d);
  cudaFree(filter_d);

#if VERBOSE
  std::cerr << "CPU polyphase_analysis took " << delta_cpu.count() << " ms" << std::endl;
  std::cerr << "GPU polyphase_analysis took " << delta_gpu.count() << " ms" << std::endl;
#endif

  REQUIRE(out_gpu.size() > 0);
  REQUIRE(out_cpu.size() > 0);
  REQUIRE(out_gpu.size() == out_cpu.size());

  bool allclose = true;
  unsigned nclose = 0;
  for (unsigned i=0; i<out_size; i++) {
    // std::cerr << "[" << out_cpu[i] << "," << out_gpu[i] << "]" << std::endl;
    if (! util::isclose(out_cpu[i], out_gpu[i], abs_tol, rel_tol)) {
      allclose = false;
    } else {
      nclose++;
    }
  }
  std::cerr << "verify_PolyphaseFilterbankPlan: " << nclose << "/" << out_size << " (" << 100 * nclose / out_size << "%) equal" << std::endl;

  REQUIRE(allclose == true);
}


// TEMPLATE_TEST_CASE (
//   "apply_filter produces same results for CPU and GPU implementations",
//   "[verify][apply_filter]",
//   float, double)
// {
//   typedef std::complex<TestType> TestTypeComplex;
//   pfb::rational os_factor(4, 3);
//
//   struct ApplyFilterShape {
//     int downsample;
//     int in_size;
//     int ntaps;
//   };
//
//   std::vector<ApplyFilterShape> shapes = {
//     ApplyFilterShape{4, 60, 12},
//     ApplyFilterShape{256, 120000, 1280},
//     ApplyFilterShape{4, 120000, 1280}, // tests k_apply_filter_reduce_sum
//   };
//
//   int downsample;
//   int in_size;
//   int ntaps;
//
//   for (unsigned idx=0; idx<shapes.size(); idx++) {
//
//     // int downsample = 4;
//     // int in_size = 60;
//     // int ntaps = 12;
//
//     int downsample = shapes[idx].downsample;
//     int in_size = shapes[idx].in_size;
//     int ntaps = shapes[idx].ntaps;
//
//     int step = os_factor.de * downsample / os_factor.nu;
//
//     int out_size = ((in_size - ntaps) / step)*downsample;
//     std::vector<TestTypeComplex> in(in_size);
//     std::vector<TestTypeComplex> filter(ntaps, TestTypeComplex(2.0, 0.0));
//     std::vector<TestTypeComplex> out_cpu(out_size, TestTypeComplex(0.0, 0.0));
//     std::vector<TestTypeComplex> out_gpu(out_size, TestTypeComplex(0.0, 0.0));
//
//     auto rando_gen = util::random<TestType>();
//
//     for (int i=0; i<in_size; i++) {
//       in[i] = TestTypeComplex(rando_gen(), rando_gen());
//     }
//
//     pfb::apply_filter(
//       in.data(), in_size, out_cpu.data(), filter.data(), ntaps, step, downsample
//     );
//
//     typedef pfb::std2cufft<TestTypeComplex> type_map;
//     typename type_map::type* in_device;
//     typename type_map::type* out_device;
//     typename type_map::type* filter_device;
//
//     size_t sz = sizeof(typename type_map::type);
//
//     cudaMalloc((void **) &in_device, in_size*sz);
//     cudaMalloc((void **) &out_device, out_size*sz);
//     cudaMalloc((void **) &filter_device, ntaps*sz);
//
//     cudaMemcpy(
//       in_device, (typename type_map::type*) in.data(), in_size*sz, cudaMemcpyHostToDevice);
//     cudaMemcpy(
//       filter_device, (typename type_map::type*) filter.data(), ntaps*sz, cudaMemcpyHostToDevice);
//
//     pfb::apply_filter_cuda(
//       in_device, in_size, out_device, filter_device, ntaps, step, downsample
//     );
//     pfb::check_error("apply_filter_cuda");
//
//     cudaMemcpy(
//       (typename type_map::type*) out_gpu.data(), out_device, out_size*sz, cudaMemcpyDeviceToHost);
//     cudaFree(in_device);
//     cudaFree(out_device);
//     cudaFree(filter_device);
//
//
//     bool allclose = true;
//     unsigned nclose = 0;
//     for (int i=0; i<out_size; i++) {
//       // std::cerr << "[" << out_cpu[i] << "," << out_gpu[i] << "]" << std::endl;
//       if (! util::isclose(out_cpu[i], out_gpu[i], abs_tol, rel_tol)) {
//         allclose = false;
//       } else {
//         nclose ++;
//       }
//     }
//     std::cerr << nclose << "/" << out_size << " (" << 100 * nclose / out_size << "%) equal" << std::endl;
//
//
//     REQUIRE(allclose == true);
//   }
//
// }
