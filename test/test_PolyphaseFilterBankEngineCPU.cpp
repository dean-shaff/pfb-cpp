#include <iostream>

#include "catch.hpp"

#include "pfb/PolyphaseFilterBankEngineCPU.hpp"

static const unsigned channels = 8;
static const pfb::util::rational os_factor (8, 7);
static const unsigned in_size = 1200;
static const unsigned filter_taps = 161;
static const unsigned step = 7;
static const unsigned filter_taps_padded = 168;
static const unsigned out_size = 1176;
static const unsigned nblocks = 147;

TEMPLATE_TEST_CASE (
  "can create pfb::PolyphaseFilterBankEngineCPU objects",
  "[unit][PolyphaseFilterBankEngineCPU]",
  std::complex<float>, std::complex<double>
)
{
  pfb::PolyphaseFilterBankEngineCPU<TestType> engine (
    channels, os_factor, in_size, filter_taps, step, filter_taps_padded, out_size, nblocks
  );
}

TEMPLATE_TEST_CASE (
  "pfb::PolyphaseFilterBankEngineCPU operates on data",
  "[unit][PolyphaseFilterBankEngineCPU]",
  std::complex<float>, std::complex<double>
)
{

  std::vector<TestType> in(in_size, TestType(0.1, 0.1));
  std::vector<TestType> out(out_size, TestType(0.0, 0.0));
  std::vector<TestType> filter(filter_taps_padded, TestType(2.0, 0.0));

  pfb::PolyphaseFilterBankEngineCPU<TestType> engine (
    channels, os_factor, in_size, filter_taps, step, filter_taps_padded, out_size, nblocks
  );

  engine.setup(out.data());
  engine(in.data(), filter.data(), out.data());

  unsigned nclose = 0;

  for (unsigned idx=0; idx<out_size; idx++) {
    if (out[idx] == TestType(0, 0)) {
      nclose++;
    }
  }

  REQUIRE(nclose < out_size);

  std::cerr << "test_PolyphaseFilterBankEngineCPU: " << nclose << "/" << out_size << " (" << (float) nclose / out_size << " %) == 0" << std::endl;

}


TEST_CASE (
  "can map between std::complex and fftw types",
  "[unit][PolyphaseFilterBankEngineCPU]"
)
{
  typename pfb::std2fftw<std::complex<float>>::type float_val;
  REQUIRE(typeid(float_val) == typeid(fftwf_complex));

  typename pfb::std2fftw<std::complex<double>>::type double_val;
  REQUIRE(typeid(double_val) == typeid(fftw_complex));
}
