#!/usr/bin/env python

from distutils.core import setup

from build import extensions, CustomBuild

with open("version.txt", "r") as fd:
    version = fd.read()


setup(
    name="pfb",
    version=version,
    description="PFB",
    author="Dean Shaff",
    author_email="dean.shaff@gmail.com",
    url="https://github.com/dean-shaff/pfb",
    packages=[
        "pfb"
    ],
    package_dir = {
        "": "python"
    },
    py_modules=["pfb.PFBPlan"],
    cmdclass={'build': CustomBuild},
    ext_modules=extensions,
)
