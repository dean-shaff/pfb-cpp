## v0.3.0

- refactor of C++ code to use PFB plans instead of a single function
- Python binding match plan based C++ refactor

## v0.3.1

- get benchmarks running
- add `setup` public method to `PolyphaseFilterBankPlan` that will setup plan independent of operating on data. For the CPU case, we have to pass the pointer to the output array that we'll be using later on.

## v0.3.2

- bench/CMakeLists.txt couldn't find cudart in CI environment

## v0.4.0

- add code coverage and TOML configurable tests.
- add version.txt
