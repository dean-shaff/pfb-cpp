Simple polyphase filterbank implementation in C++

<!-- https://github.com/swig/swig/blob/master/Tools/setup.py.tmpl -->

<!-- cmake .. -DFFTW_DIR=/fred/oz002/users/dshaff/software/install/fftw-3.3.8 -DCMAKE_CUDA_COMPILER=nvcc -DSWIG_EXECUTABLE=/apps/skylake/software/mpi/gcc/6.4.0/openmpi/3.0.0/swig/3.0.12-python-3.6.4/bin/swig -DCOMPYTE_DIR=$FRED/software/sources/compyte/ndarray -->


Desired Syntax

```c++
#include <complex>

#include "pfb/pfb.hpp"

int main () {

  typedef std::complex<float> z_float;

  int channels = 256;
  pfb::rational os_factor (4, 3);

  std::vector<z_float> in(120000);
  std::vector<z_float> coeff(1200);
  std::vector<z_float> out;

  pfb::PolyphaseFilterBankPlan<z_float> plan;

  pfb::PolyphaseFilterBankPlan::EngineCUDA engine;
  plan.set_engine(engine);

  plan.setup(
    channels, os_factor, in.size(), coeff.size());

  coeff.resize(plan.get_coeff_padded_size());
  plan.pad(coeff.data());
  out.resize(plan.get_out_size());

  plan.set_input(in.data());
  plan.set_filter_coeff(coeff.data());
  plan.set_output(out.data());

  plan();

  // or for one off:
  pfb::polyphase_filterbank<z_float>(
    in, out, coeff, channels, os_factor
  );

}




```
