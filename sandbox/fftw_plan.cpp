#include <iostream>
#include <complex>
#include <vector>

#include "fftw3.h"

int main ()
{
  int channels = 8;
  int nblocks = 10;

  std::vector<std::complex<float>> dat(channels*nblocks);

  int rank = 1; // 1D transform
  int n[] = {channels}; /* 1d transforms of length channels */
  int howmany = nblocks;
  int idist = channels;
  int odist = channels;
  int istride = 1;
  int ostride = 1;
  int *inembed = n;
  int *onembed = n;

  fftwf_complex* dat_fft = reinterpret_cast<fftwf_complex*>(dat.data());

  // fftwf_plan plan = fftwf_plan_many_dft(
  //   rank, n, howmany,
  //   dat_fft, inembed, istride, idist,
  //   dat_fft, onembed, ostride, odist,
  //   FFTW_FORWARD, FFTW_ESTIMATE);

  fftwf_plan plan = fftwf_plan_dft_1d(channels*nblocks, dat_fft, dat_fft, FFTW_FORWARD, FFTW_ESTIMATE);

  std::cerr << plan << std::endl;

  fftwf_destroy_plan(plan);
}
