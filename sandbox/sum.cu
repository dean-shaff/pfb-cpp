#include <stdio.h>
#include <iostream>
#include <chrono>
// almost all of this code is from the following articles:
// https://devblogs.nvidia.com/using-cuda-warp-level-primitives/
// https://devblogs.nvidia.com/faster-parallel-reductions-kepler/
// The latter uses the deprecated `__shfl_down` function, which does
// not take a mask value as its first parameter.

#define FULL_MASK 0xffffffff

template<typename T>
__inline__ __device__
T warpReduceSum(T val) {
  for (int offset = warpSize/2; offset > 0; offset /= 2)
    val += __shfl_down_sync(FULL_MASK, val, offset);
  return val;
}

template<typename T>
__inline__ __device__
T blockReduceSum(T val) {

  static __shared__ T shared[32]; // Shared mem for 32 partial sums
  int lane = threadIdx.x % warpSize;
  int wid = threadIdx.x / warpSize;

  val = warpReduceSum<T>(val);     // Each warp performs partial reduction

  if (lane == 0) {
    shared[wid]=val; // Write reduced value to shared memory
  }

  __syncthreads();              // Wait for all partial reductions

  // read from shared memory only if that warp existed
  val = (threadIdx.x < blockDim.x / warpSize) ? shared[lane] : 0;

  if (wid==0) {
    val = warpReduceSum<T>(val); //Final reduce within first warp
  }

  return val;
}

template<typename T>
__global__
void sum_kernel (T* x, T* y, int N)
{
  int sum = 0;
  //reduce multiple elements per thread
  for (int i = blockIdx.x * blockDim.x + threadIdx.x;
       i < N;
       i += blockDim.x * gridDim.x) {
    sum += x[i];
  }
  sum = blockReduceSum<T>(sum);
  if (threadIdx.x == 0) {
    y[blockIdx.x] = sum;
  }
}

std::chrono::time_point<std::chrono::high_resolution_clock> now () {
  return std::chrono::high_resolution_clock::now();
}


template<typename T>
void sum(T* in, T* out, int N) {
  int threads = 1024;
  int blocks = min((N + threads - 1) / threads, 1024);

  printf("threads=%i, blocks=%i\n", threads, blocks);
  sum_kernel<T><<<blocks, threads>>>(in, out, N);
  sum_kernel<T><<<1, 1024>>>(out, out, blocks);
  // return out[0];
}

template<typename T>
T sum_cpu(T *in, int N) {
  T sum = 0;
  for (int i=0; i<N; i++) {
    sum += in[i];
  }
  return sum ;
}


int main ()
{
  int N = 1<<21;
  size_t size = sizeof(float)*N;

  float *h_x, *d_x;
  float *h_y, *d_y;

  cudaMallocHost((void **) &h_x, size);
  cudaMalloc((void **) &d_x, size);

  cudaMallocHost((void **) &h_y, size);
  cudaMalloc((void **) &d_y, size);

  for (int i = 0; i < N; i++) {
    h_x[i] = (float) i;
  }

  cudaMemcpy(d_x, h_x, size, cudaMemcpyHostToDevice);
  auto start = now();
  sum(d_x, d_y, N);
  auto finish = now();
  cudaMemcpy(h_y, d_y, size, cudaMemcpyDeviceToHost);
  std::chrono::duration<double, std::milli> delta_gpu = finish - start;
  std::cerr << "CUDA: " << delta_gpu.count() << " ms" << std::endl;

  start = now();
  sum_cpu(h_x, N);
  finish = now();

  std::chrono::duration<double, std::milli> delta_cpu = finish - start;
  std::cerr << "CPU: " << delta_cpu.count() << " ms" << std::endl;

  std::cerr << "GPU " << delta_cpu.count() / delta_gpu.count() << " times faster" << std::endl;

  printf("sum=%f\n", h_y[0]);

  cudaFree(d_x);
  cudaFreeHost(h_x);

  cudaFree(d_y);
  cudaFreeHost(h_y);

}
