set(SANDBOX_SOURCES
  fftw_plan.cpp
)

add_executable(fftw_plan ${SANDBOX_SOURCES})
target_include_directories(fftw_plan PUBLIC ${FFTW_INCLUDE})
target_link_libraries(fftw_plan PUBLIC ${FFTW_LIBRARY} ${FFTWF_LIBRARY})
