#include "pfb/util.hpp"
#include "pfb/polyphase_analysis.hpp"

namespace pfb {

  template<typename T>
  void apply_filter (
    const T* in,
    int in_size,
    T* out,
    const T* filter_coeff,
    int filter_taps,
    int step,
    int downsample
  )
  {
    auto roll = [] (int idx, int size, int roll_by) -> int {
      int idx_rolled = idx + (size - roll_by);
      if (idx_rolled >= size) {
        return idx_rolled - size;
      } else {
        return idx_rolled;
      }
    };


    int taps_per_sample = filter_taps / downsample;
    int nblocks = (in_size - filter_taps) / step;

    const T* in_ptr = in;
    T* out_ptr = out;

    int block_step; // size of step in input samples
    int roll_idx; // this determines by how much we roll ``idx``
    int idx; // the index of the input and filter coefficients buffers

    // #pragma omp parallel for
    for (int iblock=0; iblock<nblocks; iblock++)
    {
      block_step = iblock*step;
      roll_idx = block_step - (block_step/downsample)*downsample;

      for (int ichan=0; ichan<downsample; ichan++) {
        for (int i=0; i<taps_per_sample; i++) {
          idx = ichan + i*downsample;
          idx = roll(idx, filter_taps, roll_idx);
          out_ptr[ichan] += in_ptr[idx] * filter_coeff[idx];
        }
      }
      for (int ichan=0; ichan<downsample; ichan++) {
        out_ptr[ichan] *= downsample;
      }
      in_ptr += step;
      out_ptr += downsample;
    }
  }

  template void apply_filter<std::complex<float>> (
    const std::complex<float>* in,
    int in_size,
    std::complex<float>* out,
    const std::complex<float>* filter_coeff,
    int filter_taps,
    int step,
    int downsample
  );

  template void apply_filter<std::complex<double>> (
    const std::complex<double>* in,
    int in_size,
    std::complex<double>* out,
    const std::complex<double>* filter_coeff,
    int filter_taps,
    int step,
    int downsample
  );

  template<typename T>
  void polyphase_analysis (
    const std::vector<T>& in,
    std::vector<T>& out,
    std::vector<T>& filter_coeff,
    int channels,
    const rational& os_factor
  )
  {

    int in_size = in.size();
    int filter_taps = filter_coeff.size();

    int filter_taps_padded = calc_filter_taps_padded(
      filter_taps, channels);

    if ((int) filter_coeff.size() != filter_taps_padded)
    {
      filter_coeff.resize(filter_taps_padded);
    }

    int out_size = calc_out_size(
      in_size,
      channels,
      filter_taps_padded,
      os_factor);

    if ((int) out.size() != out_size) {
      out.resize(out_size);
    }

    polyphase_analysis(
      in.data(), in_size, out.data(), filter_coeff.data(),
      filter_coeff.size(), channels, os_factor
    );

  }

  template void polyphase_analysis<std::complex<float>> (
    const std::vector<std::complex<float>>& in,
    std::vector<std::complex<float>>& out,
    std::vector<std::complex<float>>& filter_coeff,
    int channels,
    const rational& os_factor
  );

  template void polyphase_analysis<std::complex<double>> (
    const std::vector<std::complex<double>>& in,
    std::vector<std::complex<double>>& out,
    std::vector<std::complex<double>>& filter_coeff,
    int channels,
    const rational& os_factor
  );

  template<typename T>
  void polyphase_analysis (
    const T* in,
    int in_size,
    T* out,
    const T* filter_coeff,
    int filter_taps,
    int channels,
    const rational& os_factor
  )
  {
    if ((filter_taps) % channels != 0) {
      throw "pfb::polyphase_analysis Number of filter taps must be a multiple of the number of channels!";
    }

    int step = os_factor.de * channels / os_factor.nu;
    int nblocks = (in_size - filter_taps) / step;

  #if VERBOSE
    std::cerr << "pfb::polyphase_analysis in_size=" << in_size << std::endl;
    std::cerr << "pfb::polyphase_analysis filter_taps=" << filter_taps << std::endl;
    std::cerr << "pfb::polyphase_analysis channels=" << channels << std::endl;
    std::cerr << "pfb::polyphase_analysis step=" << step << std::endl;
    std::cerr << "pfb::polyphase_analysis nblocks=" << nblocks << std::endl;
  #endif

    typedef std2fftw<T> type_map;

    // setup fft plan
    typename type_map::type* in_fft = reinterpret_cast<typename type_map::type*>(out);
    int rank = 1; // 1D transform
    int n[] = {(int) channels}; /* 1d transforms of length 10 */
    int howmany = nblocks;
    int idist = channels;
    int odist = channels;
    int istride = 1;
    int ostride = 1;
    int *inembed = n, *onembed = n;

    typename type_map::plan_type plan = type_map::plan_many_dft(
      rank, n, howmany,
      in_fft, inembed, istride, idist,
      in_fft, onembed, ostride, odist,
      FFTW_FORWARD, FFTW_ESTIMATE);

  #if VERBOSE
    std::cerr << "pfb::polyphase_analysis applying filter" << std::endl;
  #endif

    apply_filter<T>(
      in, in_size, out, filter_coeff, filter_taps, step, channels
    );

  #if VERBOSE
    std::cerr << "pfb::polyphase_analysis doing FFT" << std::endl;
  #endif

    type_map::execute(plan);
    type_map::destroy_plan(plan);

  #if VERBOSE
    std::cerr << "pfb::polyphase_analysis done" << std::endl;
  #endif
  }


  template void polyphase_analysis (
    const std::complex<float>* in,
    int in_size,
    std::complex<float>* out,
    const std::complex<float>* filter_coeff,
    int filter_taps,
    int channels,
    const rational& os_factor
  );


  template void polyphase_analysis (
    const std::complex<double>* in,
    int in_size,
    std::complex<double>* out,
    const std::complex<double>* filter_coeff,
    int filter_taps,
    int channels,
    const rational& os_factor
  );




}
