#include <iostream>

#include "pfb/util.hpp"
#include "pfb/polyphase_analysis_kernels.cuh"
#include "pfb/polyphase_analysis_cuda.hpp"


namespace pfb {

  typedef std2cufft<std::complex<float>> type_map_float;
  typedef std2cufft<std::complex<double>> type_map_double;

  template<typename T>
  void polyphase_analysis_cuda (
    std::vector<T>& in,
    std::vector<T>& out,
    std::vector<T>& filter_coeff,
    int channels,
    rational os_factor
  )
  {
    int in_size = in.size();
    int filter_taps = filter_coeff.size();

    int filter_taps_padded = calc_filter_taps_padded(
      filter_taps, channels);

    if (filter_coeff.size() != filter_taps_padded)
    {
      filter_coeff.resize(filter_taps_padded);
    }

    int out_size = calc_out_size(
      in_size,
      channels,
      filter_taps_padded,
      os_factor);

    if (out.size() != out_size) {
      out.resize(out_size);
    }

    typedef std2cufft<T> type_map;

    typename type_map::type* in_device;
    typename type_map::type* out_device;
    typename type_map::type* filter_coeff_device;

    size_t sz = sizeof(typename type_map::type);

    cudaMalloc((void **) &in_device, in_size*sz);
    cudaMalloc((void **) &out_device, out_size*sz);
    cudaMalloc((void **) &filter_coeff_device, filter_taps*sz);

    cudaMemcpy(
      in_device, (typename type_map::type*) in.data(), in_size*sz, cudaMemcpyHostToDevice);
    cudaMemcpy(
      filter_coeff_device, (typename type_map::type*) filter_coeff.data(), filter_taps*sz, cudaMemcpyHostToDevice);

    polyphase_analysis_cuda<typename type_map::type>(
      in_device, in_size, out_device, filter_coeff_device,
      filter_taps, channels, os_factor
    );

    cudaMemcpy(
      (typename type_map::type*) out.data(), out_device, out_size*sz, cudaMemcpyDeviceToHost);
    cudaFree(in_device);
    cudaFree(out_device);
    cudaFree(filter_coeff_device);
  }

  template void polyphase_analysis_cuda<std::complex<float>> (
    std::vector<std::complex<float>>& in,
    std::vector<std::complex<float>>& out,
    std::vector<std::complex<float>>& filter_coeff,
    int channels,
    rational os_factor
  );

  template void polyphase_analysis_cuda<std::complex<double>> (
    std::vector<std::complex<double>>& in,
    std::vector<std::complex<double>>& out,
    std::vector<std::complex<double>>& filter_coeff,
    int channels,
    rational os_factor
  );

  template<typename T>
  void polyphase_analysis_cuda (
    T* in,
    int in_size,
    T* out,
    T* filter_coeff,
    int filter_taps,
    int channels,
    rational os_factor
  )
  {
    if ((filter_taps) % channels != 0) {
      throw "pfb::polyphase_analysis_cuda: Number of filter taps must be a multiple of the number of channels!";
    }

    int step = os_factor.de * channels / os_factor.nu;
    int nblocks = (in_size - filter_taps) / step;

  #if VERBOSE
    std::cerr << "pfb::polyphase_analysis_cuda: in_size=" << in_size << std::endl;
    std::cerr << "pfb::polyphase_analysis_cuda: filter_taps=" << filter_taps << std::endl;
    std::cerr << "pfb::polyphase_analysis_cuda: channels=" << channels << std::endl;
    std::cerr << "pfb::polyphase_analysis_cuda: step=" << step << std::endl;
    std::cerr << "pfb::polyphase_analysis_cuda: nblocks=" << nblocks << std::endl;
  #endif

    typedef cufftTypeMap<T> cufft_type_map;

    // setup fft plan
    // T* in_fft = reinterpret_cast<typename type_map::type*>(out);
    int rank = 1; // 1D transform
    int n[] = {(int) channels}; /* 1d transforms of length 10 */
    int howmany = nblocks;
    int idist = channels;
    int odist = channels;
    int istride = 1;
    int ostride = 1;
    int *inembed = n, *onembed = n;

    cufftHandle plan;
    cufftResult success;

    success = cufftPlanMany(
      &plan, rank, n,
      inembed, istride, idist,
      onembed, ostride, odist,
      cufft_type_map::flag, howmany);

    if (success != CUFFT_SUCCESS) {
      std::cerr << "pfb::polyphase_analysis_cuda: failed to create plan, success=" << success << std::endl;
      throw "pfb::polyphase_analysis_cuda: Failed to create cuFFT plan";
    }

  #if VERBOSE
    std::cerr << "pfb::polyphase_analysis_cuda: applying filter" << std::endl;
  #endif

    apply_filter_cuda<T>(
      in, in_size, out, filter_coeff, filter_taps, step, channels
    );

  #if VERBOSE
    std::cerr << "pfb::polyphase_analysis_cuda: doing FFT" << std::endl;
  #endif

    success = cufft_type_map::execute(plan, out, out, CUFFT_FORWARD);
    success = cufftDestroy(plan);

  #if VERBOSE
    std::cerr << "pfb::polyphase_analysis_cuda: done" << std::endl;
  #endif
  }


  template void polyphase_analysis_cuda<type_map_float::type> (
    type_map_float::type* in,
    int in_size,
    type_map_float::type* out,
    type_map_float::type* filter_coeff,
    int filter_taps,
    int channels,
    rational os_factor
  );

  template void polyphase_analysis_cuda<type_map_double::type> (
    type_map_double::type* in,
    int in_size,
    type_map_double::type* out,
    type_map_double::type* filter_coeff,
    int filter_taps,
    int channels,
    rational os_factor
  );

  template<typename T>
  void apply_filter_cuda (
    T* in,
    int in_size,
    T* out,
    T* filter_coeff,
    int filter_taps,
    int step,
    int downsample
  )
  {
    int taps_per_sample = filter_taps / downsample;
    int nblocks = (in_size - filter_taps) / step;

  #if VERBOSE
    std::cerr << "pfb::apply_filter_cuda: taps_per_sample=" << taps_per_sample << std::endl;
    std::cerr << "pfb::apply_filter_cuda: nblocks=" << nblocks << std::endl;
  #endif


    if (taps_per_sample <= 32) {

      int nthreads = 1024;
      int nblocks_x = 1;

      if (nblocks < nthreads) {
        nthreads = nblocks;
      }

      if (nthreads >= 1024) {
        nblocks_x = nblocks / nthreads;
      }

      dim3 grid (nblocks_x, downsample, 1);
      dim3 block (nthreads, 1, 1);

      k_apply_filter<T><<<grid, block>>>(
        in,
        out,
        filter_coeff,
        filter_taps,
        step,
        downsample,
        nblocks,
        taps_per_sample
      );
    } else {
      int nthreads = taps_per_sample;
      if (nthreads > 1024) {
        nthreads = 1024;
      }

      dim3 grid (nblocks, downsample, 1);
      dim3 block (nthreads, 1, 1);

      size_t shared_bytes = nthreads / 32;
      if (shared_bytes == 0) {
        shared_bytes++;
      }
      shared_bytes *= sizeof(T);

      k_apply_filter_reduce_sum<T><<<grid, block, shared_bytes>>>(
        in,
        out,
        filter_coeff,
        filter_taps,
        step,
        downsample,
        nblocks,
        taps_per_sample
      );
    }
  }

  template void apply_filter_cuda<type_map_float::type> (
    type_map_float::type* in,
    int in_size,
    type_map_float::type* out,
    type_map_float::type* filter_coeff,
    int filter_taps,
    int step,
    int downsample
  );

  template void apply_filter_cuda<type_map_double::type> (
    type_map_double::type* in,
    int in_size,
    type_map_double::type* out,
    type_map_double::type* filter_coeff,
    int filter_taps,
    int step,
    int downsample
  );


}
