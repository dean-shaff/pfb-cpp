#ifndef __polyphase_analysis_hpp
#define __polyphase_analysis_hpp

#include <vector>
#include <complex>
#include <string.h>
#include <iostream>
#include <functional>

#include "fftw3.h"

#include "util.hpp"

namespace pfb {

  template<class T>
  struct std2fftw;

  template<>
  struct std2fftw<std::complex<double>> {
    typedef fftw_complex type;
    typedef fftw_plan plan_type;

    static plan_type plan_many_dft (
      int rank, const int *n, int howmany,
      type *in, const int *inembed,
      int istride, int idist,
      type *out, const int *onembed,
      int ostride, int odist,
      int sign, unsigned flags) {
        return fftw_plan_many_dft(
          rank, n, howmany, in, inembed,
          istride, idist, out, onembed,
          ostride, odist, sign, flags);
    };

    static void execute (plan_type plan) {
      fftw_execute(plan);
    };

    static void destroy_plan (plan_type plan) {
      fftw_destroy_plan(plan);
    };
  };

  template<>
  struct std2fftw<std::complex<float>> {
    typedef fftwf_complex type;
    typedef fftwf_plan plan_type;

    static plan_type plan_many_dft (
      int rank, const int *n, int howmany,
      type *in, const int *inembed,
      int istride, int idist,
      type *out, const int *onembed,
      int ostride, int odist,
      int sign, unsigned flags) {
        return fftwf_plan_many_dft(
          rank, n, howmany, in, inembed,
          istride, idist, out, onembed,
          ostride, odist, sign, flags);
    };

    static void execute (plan_type plan) {
      fftwf_execute(plan);
    };

    static void destroy_plan (plan_type plan) {
      fftwf_destroy_plan(plan);
    };
  };

  template<typename T>
  void apply_filter (
    const T* in,
    int in_size,
    T* out,
    const T* filter_coeff,
    int filter_taps,
    int step,
    int downsample
  );

  template<typename T>
  void polyphase_analysis (
    const std::vector<T>& in,
    std::vector<T>& out,
    std::vector<T>& filter_coeff,
    int channels,
    const rational& os_factor
  );

  template<typename T>
  void polyphase_analysis (
    const T* in,
    int in_size,
    T* out,
    const T* filter_coeff,
    int filter_taps,
    int channels,
    const rational& os_factor
  );
}

#endif
