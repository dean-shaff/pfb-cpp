#ifndef __polyphase_analysis_cuda_cuh
#define __polyphase_analysis_cuda_cuh

#include <complex>
#include <vector>

#include <cuda_runtime.h>
#include <cufft.h>

#include "util.hpp"

namespace pfb {

  template<typename T>
  struct std2cufft;

  template<>
  struct std2cufft<std::complex<float>> {
    typedef cufftComplex type;
  };

  template<>
  struct std2cufft<std::complex<double>> {
    typedef cufftDoubleComplex type;
  };


  template<typename T>
  struct cufftTypeMap;

  template<>
  struct cufftTypeMap<cufftComplex> {
    typedef cufftComplex type;
    static const cufftType flag = CUFFT_C2C;
    static cufftResult execute (
      cufftHandle plan, type *idata,
      type *odata, int direction
    )
    {
      return cufftExecC2C(plan, idata, odata, direction);
    }
  };

  template<>
  struct cufftTypeMap<cufftDoubleComplex> {
    typedef cufftDoubleComplex type;
    static const cufftType flag = CUFFT_Z2Z;
    static cufftResult execute (
      cufftHandle plan, type *idata,
      type *odata, int direction
    )
    {
      return cufftExecZ2Z(plan, idata, odata, direction);
    }
  };


  template<typename T>
  void apply_filter_cuda (
    T* in,
    int in_size,
    T* out,
    T* filter_coeff,
    int filter_taps,
    int step,
    int downsample
  );


  template<typename T>
  void polyphase_analysis_cuda (
    std::vector<T>& in,
    std::vector<T>& out,
    std::vector<T>& filter_coeff,
    int channels,
    rational os_factor
  );

  template<typename T>
  void polyphase_analysis_cuda (
    T* in,
    int in_size,
    T* out,
    T* filter_coeff,
    int filter_taps,
    int channels,
    rational os_factor
  );
}

#endif
