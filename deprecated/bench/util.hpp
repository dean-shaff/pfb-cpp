#ifndef __util_bench_hpp
#define __util_bench_hpp

#include <functional>
#include <vector>
#include <complex>
#include <chrono>

#include "pfb/pfb.hpp"

namespace util {

  template<typename T>
  using delta = std::chrono::duration<double, T>;

  typedef std::chrono::time_point<std::chrono::high_resolution_clock> time_point;

  time_point now ();

  template<typename Ratio, typename T>
  delta<Ratio> bench_polyphase_analysis_cpu (
    int niter, int in_size, int ntaps, int channels, const pfb::rational& os_factor
  );

  #if HAVE_CUDA
  template<typename Ratio, typename T>
  delta<Ratio> bench_polyphase_analysis_cuda (
    int niter, int in_size, int ntaps, int channels, const pfb::rational& os_factor
  );
  #endif

}

inline util::time_point util::now () {
  return std::chrono::high_resolution_clock::now();
}


template<typename Ratio, typename T>
util::delta<Ratio> util::bench_polyphase_analysis_cpu (
  int niter, int in_size, int ntaps, int channels, const pfb::rational& os_factor
)
{
  int ntaps_padded = pfb::calc_filter_taps_padded(ntaps, channels);
  int out_size = pfb::calc_out_size(in_size, channels, ntaps_padded, os_factor);

  std::vector<std::complex<T>> in(in_size, std::complex<T>(1.0, 1.0));
  std::vector<std::complex<T>> out(out_size);
  std::vector<std::complex<T>> filter(ntaps_padded, std::complex<T>(2.0, 0.0));

  auto t0 = util::now();
  for (int i=0; i<niter; i++) {
    pfb::polyphase_analysis<std::complex<T>>(
      in.data(), in_size, out.data(), filter.data(),
      ntaps_padded, channels, os_factor
    );
  }
  auto t1 = util::now();
  util::delta<Ratio> delta = t1 - t0;
  return delta;
}

#if HAVE_CUDA
template<typename Ratio, typename T>
util::delta<Ratio> util::bench_polyphase_analysis_cuda (
  int niter, int in_size, int ntaps, int channels, const pfb::rational& os_factor
)
{
  int ntaps_padded = pfb::calc_filter_taps_padded(ntaps, channels);
  int out_size = pfb::calc_out_size(in_size, channels, ntaps_padded, os_factor);

  std::vector<std::complex<T>> in(in_size, std::complex<T>(1.0, 1.0));
  std::vector<std::complex<T>> filter(ntaps_padded, std::complex<T>(2.0, 0.0));

  typedef pfb::std2cufft<std::complex<T>> type_map;

  typename type_map::type* in_device;
  typename type_map::type* out_device;
  typename type_map::type* filter_coeff_device;

  size_t sz = sizeof(typename type_map::type);

  cudaMalloc((void **) &in_device, in_size*sz);
  cudaMalloc((void **) &out_device, out_size*sz);
  cudaMalloc((void **) &filter_coeff_device, ntaps_padded*sz);

  cudaMemcpy(
    in_device, (typename type_map::type*) in.data(), in_size*sz, cudaMemcpyHostToDevice);
  cudaMemcpy(
    filter_coeff_device, (typename type_map::type*) filter.data(), ntaps_padded*sz, cudaMemcpyHostToDevice);
  cudaDeviceSynchronize();

  // cudaEvent_t start, stop;
  // float elapsed_milli = 0;
  // cudaEventCreate(&start);
  // cudaEventCreate(&stop);

  pfb::polyphase_analysis_cuda<typename type_map::type>(
    in_device, in_size, out_device, filter_coeff_device,
    ntaps_padded, channels, os_factor
  );
  cudaDeviceSynchronize();


  auto t0 = util::now();
  util::time_point t1;
  util::delta<Ratio> delta1;
  for (int i=0; i<niter; i++) {
    t1 = util::now();
    // cudaEventRecord(start);
    pfb::polyphase_analysis_cuda<typename type_map::type>(
      in_device, in_size, out_device, filter_coeff_device,
      ntaps_padded, channels, os_factor
    );
    // cudaEventRecord(stop);
    cudaDeviceSynchronize();
    delta1 = util::now() - t1;
    // cudaEventSynchronize(stop);
    // cudaEventElapsedTime(&elapsed_milli, start, stop);
    // std::cerr << "polyphase_analysis_cuda: loop " << i << " " << delta1.count() << std::endl;
    // std::cerr << "polyphase_analysis_cuda: loop " << i << " " << elapsed_milli << std::endl;
  }
  util::delta<Ratio> delta = util::now() - t0;

  cudaFree(in_device);
  cudaFree(out_device);
  cudaFree(filter_coeff_device);

  return delta;

}
#endif

// template<typename T, typename dtype>
// std::chrono::duration<double, T> util::bench_pfb_polyphase_analysis(
//   util::polyphase_analysis_sig<std::complex<dtype>> func,
//   int niter, int in_size, int ntaps, int channels, pfb::rational& os_factor
// )
// {
//   std::vector<std::complex<dtype>> in(in_size, std::complex<dtype>(1.0, 1.0));
//   std::vector<std::complex<dtype>> out;
//   std::vector<std::complex<dtype>> filter(ntaps, std::complex<dtype>(2.0, 0.0));
//
//   auto t0 = util::now();
//   for (int i=0; i<niter; i++) {
//     func(in, out, filter, channels, os_factor);
//   }
//   auto t1 = util::now();
//   std::chrono::duration<double, T> delta = t1 - t0;
//   return delta;
// }

#endif
