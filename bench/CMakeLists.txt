
set(BENCH_SOURCES
  ${BENCH_DIR}/bench_polyphase_analysis.cpp
)

add_executable(bench_main ${BENCH_SOURCES})
target_include_directories(bench_main PRIVATE ${INCLUDE_DIR})
target_include_directories(bench_main PRIVATE ${BENCH_DIR})
target_link_libraries(bench_main PRIVATE pfbcpp)
if (HAVE_CUDA)
  target_link_libraries(bench_main PRIVATE ${CUDART_LIBRARY})
endif()
target_compile_definitions(bench_main PRIVATE HAVE_CUDA=${HAVE_CUDA} VERBOSE=${DEBUG})
