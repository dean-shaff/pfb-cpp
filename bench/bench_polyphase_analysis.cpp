#include <iostream>
#include <cstring>
#include <cstdlib>
#include <complex>
#include <vector>
#include <chrono>

#include "pfb/PolyphaseFilterBankPlan.hpp"
#if HAVE_CUDA
#include "pfb/PolyphaseFilterBankEngineCUDA.hpp"
#include "pfb/util/cuda_container.hpp"
#endif

std::chrono::time_point<std::chrono::high_resolution_clock> now () {
  return std::chrono::high_resolution_clock::now();
}


struct Options {
  int in_size;
  int channels;
  int ntaps;
  int niter;
};

void parse_args (Options& options, int argv, char** argc)
{
  char** argc_end = argc + argv;
  char** argc_begin = argc;

  char* cand;

  while (argc_begin != argc_end) {
    cand = *argc_begin;
    if (std::strcmp (cand, "-n") == 0 || std::strcmp(cand, "--niter") == 0) {
      argc_begin++;
      options.niter = std::atoi(*argc_begin);
    } else if (std::strcmp (cand, "-in") == 0 || std::strcmp(cand, "--in-size") == 0) {
      argc_begin++;
      options.in_size = std::atoi(*argc_begin);
    } else if (std::strcmp (cand, "-c") == 0 || std::strcmp(cand, "--channels") == 0) {
      argc_begin++;
      options.channels = std::atoi(*argc_begin);
    } else if (std::strcmp (cand, "-t") == 0 || std::strcmp(cand, "--ntaps") == 0) {
      argc_begin++;
      options.ntaps = std::atoi(*argc_begin);
    }
    argc_begin++;
  }
}

template<typename stdtype>
void benchmark (const Options& options) {

  typedef typename pfb::std2cufft<stdtype>::type cuffttype;

  pfb::util::rational os_factor(4, 3);

  std::vector<stdtype> in(options.in_size);

  std::vector<stdtype> filter(options.ntaps);

  std::vector<stdtype> out;

  auto t0 = std::chrono::high_resolution_clock::now();
  auto t1 = t0;
  pfb::PolyphaseFilterBankPlan<stdtype> plan(
    options.channels, os_factor, options.in_size, options.ntaps);

  filter.resize(plan.get_filter_taps_padded());
  out.resize(plan.get_out_size());

  plan.setup(out.data());

  std::chrono::duration<double, std::ratio<1>> delta_cpu_setup = now() - t0;
  std::cerr << "polyphase_analysis CPU setup took " << delta_cpu_setup.count() << " s " << std::endl;

  t0 = now();
  for (int iter=0; iter<options.niter; iter++) {
    plan(in.data(), filter.data(), out.data());
  }
  std::chrono::duration<double, std::ratio<1>> delta_cpu = now() - t0;
  std::chrono::duration<double, std::ratio<1>> delta_cpu_total = now() - t1;

  double time_total_cpu = delta_cpu.count();
  double time_total_cpu_per_loop = time_total_cpu / options.niter;

  std::cerr << "polyphase_analysis CPU took " << time_total_cpu_per_loop << " s per loop" << std::endl;
  std::cerr << "polyphase_analysis CPU took " << delta_cpu_total.count() << " s in total" << std::endl;

#if HAVE_CUDA

  int driver_version;
  cudaDriverGetVersion(&driver_version);

  int runtime_version;
  cudaRuntimeGetVersion(&runtime_version);

  std::cerr << "CUDA Runtime version: " << runtime_version << std::endl;
  std::cerr << "CUDA Driver version: " << driver_version << std::endl;


  if (driver_version < runtime_version || (runtime_version == 0 && driver_version == 0)) {
    return;
  }


  t1 = now();
  t0 = now();
  pfb::PolyphaseFilterBankPlan<
    cuffttype,
    pfb::PolyphaseFilterBankEngineCUDA<cuffttype>
  > plan_gpu(
    options.channels, os_factor, options.in_size, options.ntaps);
  plan_gpu.setup();
  cudaDeviceSynchronize();
  std::chrono::duration<double, std::ratio<1>> delta_cuda_setup = now() - t0;
  std::cerr << "polyphase_analysis CUDA setup took " << delta_cuda_setup.count() << " s " << std::endl;
  pfb::util::CUDAContainer<cuffttype> in_gpu(in);
  pfb::util::CUDAContainer<cuffttype> filter_gpu(filter);
  pfb::util::CUDAContainer<cuffttype> out_gpu(out);
  cudaDeviceSynchronize();

  t0 = now();
  for (int iter=0; iter<options.niter; iter++) {
    plan_gpu(in_gpu.data(), filter_gpu.data(), out_gpu.data());
    cudaDeviceSynchronize();
  }
  std::chrono::duration<double, std::ratio<1>> delta_cuda = now() - t0;
  std::chrono::duration<double, std::ratio<1>> delta_cuda_total = now() - t1;

  double time_total_cuda = delta_cuda.count();
  double time_total_cuda_per_loop = time_total_cuda / options.niter;
  std::cerr << "polyphase_analysis CUDA took " << time_total_cuda_per_loop << " s per loop" << std::endl;
  std::cerr << "polyphase_analysis CUDA took " << delta_cuda_total.count() << " s in total" << std::endl;

  if (time_total_cpu > time_total_cuda) {
    std::cerr << "polyphase_analysis CUDA " << time_total_cpu / time_total_cuda << " times faster" << std::endl;
  } else {
    std::cerr << "polyphase_analysis CPU " << time_total_cuda / time_total_cpu << " times faster" << std::endl;
  }
#endif

}

int main (int argv, char** argc) {
  Options options{1200, 8, 120, 100};

  parse_args (options, argv, argc);
  std::cerr << "std::complex<float> benchmark" << std::endl;
  benchmark<std::complex<float>>(options);
  std::cerr << "std::complex<double> benchmark" << std::endl;
  benchmark<std::complex<double>>(options);

}
