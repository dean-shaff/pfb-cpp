#ifndef __PolyphaseFilterBankEngineCUDA_hpp
#define __PolyphaseFilterBankEngineCUDA_hpp

#include <iostream>
#include <complex>
#include <vector>

#include <cuda_runtime.h>
#include <cufft.h>

#include "pfb/PolyphaseFilterBankEngine.hpp"

namespace pfb {

  template<typename T>
  struct std2cufft;

  template<>
  struct std2cufft<std::complex<float>> {
    typedef cufftComplex type;
  };

  template<>
  struct std2cufft<std::complex<double>> {
    typedef cufftDoubleComplex type;
  };


  template<typename T>
  struct cufftTypeMap;

  template<>
  struct cufftTypeMap<cufftComplex> {
    typedef cufftComplex type;
    static const cufftType flag = CUFFT_C2C;
    static cufftResult execute (
      cufftHandle plan, type *idata,
      type *odata, int direction
    )
    {
      return cufftExecC2C(plan, idata, odata, direction);
    }
  };

  template<>
  struct cufftTypeMap<cufftDoubleComplex> {
    typedef cufftDoubleComplex type;
    static const cufftType flag = CUFFT_Z2Z;
    static cufftResult execute (
      cufftHandle plan, type *idata,
      type *odata, int direction
    )
    {
      return cufftExecZ2Z(plan, idata, odata, direction);
    }
  };

  template<typename dtype>
  class PolyphaseFilterBankEngineCUDA : public PolyphaseFilterBankEngine<dtype> {

  public:

    typedef cufftTypeMap<dtype> cufft_type_map;

    ~PolyphaseFilterBankEngineCUDA () {
      #if VERBOSE
        std::cerr << "pfb::PolyphaseFilterBankEngineCUDA::~PolyphaseFilterBankEngineCUDA" << std::endl;
      #endif

      if (this->is_setup) {
        cufftDestroy(plan);
        // cufftResult success = cufftDestroy(plan);
        // if (success != CUFFT_SUCCESS) {
        //   throw "Failed to destroy cuFFT plan";
        // }
      }
    }

    PolyphaseFilterBankEngineCUDA (
      unsigned _channels,
      pfb::util::rational _os_factor,
      unsigned _in_size,
      unsigned _filter_taps,
      unsigned _step,
      unsigned _filter_taps_padded,
      unsigned _out_size,
      unsigned _nblocks
    );

    void setup (dtype* _output=nullptr);

    void operator() (
      dtype* _input,
      dtype* _filter_coeff,
      dtype* _output);

    static void print_array (const float* arr, unsigned size); 

  private:

    cufftHandle plan;

    void apply_filter (
      dtype* input,
      dtype* filter_coeff,
      dtype* output
    );

  };
}




#endif
