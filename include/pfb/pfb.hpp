#ifndef __pfb_hpp
#define __pfb_hpp

#include <vector>

#include "util/util.hpp"

namespace pfb {
  template<typename dtype>
  void polyphase_filterbank (
    const std::vector<dtype>& input,
    std::vector<dtype>& output,
    const std::vector<dtype>& filter_coeff,
    unsigned channels,
    const pfb::util::rational& os_factor);
}





#endif
