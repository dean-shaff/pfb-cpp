#ifndef __PolyphaseFilterBankPlan_hpp
#define __PolyphaseFilterBankPlan_hpp

#include "pfb/PolyphaseFilterBankEngineCPU.hpp"

namespace pfb {

template<typename dtype, typename EngineType=PolyphaseFilterBankEngineCPU<dtype>>
class PolyphaseFilterBankPlan {

public:

  PolyphaseFilterBankPlan (
    unsigned _channels,
    const pfb::util::rational& _os_factor,
    unsigned _in_size,
    unsigned _filter_taps);

  ~PolyphaseFilterBankPlan ();

  void operator() (dtype* _input, dtype* _filter_coeff, dtype* _output);

  template<class ... Types>
  void setup (Types ... args)
  {
    engine->setup (args...);
    engine_setup = true;
  }


  unsigned get_channels () const { return channels; }
  pfb::util::rational get_os_factor () const { return os_factor; }
  unsigned get_in_size () const { return in_size; }
  unsigned get_filter_taps () const { return filter_taps; }

  unsigned get_filter_taps_padded () const { return filter_taps_padded; }
  unsigned get_out_size () const { return out_size; }
  unsigned get_step () const { return step; }
  unsigned get_nblocks () const { return nblocks; }

protected:

  EngineType* engine;

  unsigned channels;
  pfb::util::rational os_factor;
  unsigned in_size;
  unsigned filter_taps;

  unsigned step;
  unsigned filter_taps_padded;
  unsigned out_size;
  unsigned nblocks;

  bool engine_setup;

private:

  dtype* input;
  dtype* output;
  dtype* filter_coeff;
};

}


template<typename dtype, typename EngineType>
pfb::PolyphaseFilterBankPlan<dtype, EngineType>::PolyphaseFilterBankPlan (
  unsigned _channels,
  const pfb::util::rational& _os_factor,
  unsigned _in_size,
  unsigned _filter_taps
) : channels(_channels), os_factor(_os_factor), in_size(_in_size), filter_taps(_filter_taps)
{
  #if VERBOSE
    std::cerr << "pfb::PolyphaseFilterBankPlan::PolyphaseFilterBankPlan" << std::endl;
  #endif
  pfb::util::four_tuple res = pfb::util::calc_out_size(
    channels, os_factor, in_size, filter_taps);

  filter_taps_padded = std::get<0>(res);
  step = std::get<1>(res);
  nblocks = std::get<2>(res);
  out_size = std::get<3>(res);

  engine = new EngineType(
    channels, os_factor, in_size, filter_taps,
    step, filter_taps_padded, out_size, nblocks);
  engine_setup = false;
}

template<typename dtype, typename EngineType>
void pfb::PolyphaseFilterBankPlan<dtype, EngineType>::operator() (
  dtype* _input, dtype* _filter_coeff, dtype* _output
)
{
  #if VERBOSE
    std::cerr << "pfb::PolyphaseFilterBankPlan::operator()" << std::endl;
  #endif

  if (! engine_setup) {
    engine->setup (_output);
    engine_setup = true;
  }

  engine->operator() (_input, _filter_coeff, _output);
}

template<typename dtype, typename EngineType>
pfb::PolyphaseFilterBankPlan<dtype, EngineType>::~PolyphaseFilterBankPlan () {
  #if VERBOSE
    std::cerr << "pfb::PolyphaseFilterBankPlan::~PolyphaseFilterBankPlan" << std::endl;
  #endif
  delete engine;
}


#endif
