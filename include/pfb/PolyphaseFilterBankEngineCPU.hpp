#ifndef __PolyphaseFilterBankEngineCPU_hpp
#define __PolyphaseFilterBankEngineCPU_hpp

#include <iostream>
#include <complex>

#include "fftw3.h"

#include "pfb/PolyphaseFilterBankEngine.hpp"

namespace pfb {

  template<class T>
  struct std2fftw;

  template<>
  struct std2fftw<std::complex<double>> {
    typedef fftw_complex type;
    typedef fftw_plan plan_type;

    template<class ... Types>
    static plan_type plan_many_dft (Types ... args) {
        return fftw_plan_many_dft(args...);
    }

    static void execute (plan_type plan) {
      fftw_execute(plan);
    };

    static void destroy_plan (plan_type plan) {
      fftw_destroy_plan(plan);
    };
  };

  template<>
  struct std2fftw<std::complex<float>> {
    typedef fftwf_complex type;
    typedef fftwf_plan plan_type;

    template<class ... Types>
    static plan_type plan_many_dft (Types ... args) {
      return fftwf_plan_many_dft(args...);
    }

    static void execute (plan_type plan) {
      fftwf_execute(plan);
    };

    static void destroy_plan (plan_type plan) {
      fftwf_destroy_plan(plan);
    };
  };

  template<typename dtype>
  class PolyphaseFilterBankEngineCPU : public PolyphaseFilterBankEngine<dtype> {

  public:

    typedef std2fftw<dtype> type_map;

    ~PolyphaseFilterBankEngineCPU () {
      #if VERBOSE
        std::cerr << "pfb::PolyphaseFilterBankEngineCPU::~PolyphaseFilterBankEngineCPU" << std::endl;
      #endif
      if (this->is_setup) {
        type_map::destroy_plan(plan);
      }
    }

    PolyphaseFilterBankEngineCPU (
      unsigned _channels,
      pfb::util::rational _os_factor,
      unsigned _in_size,
      unsigned _filter_taps,
      unsigned _step,
      unsigned _filter_taps_padded,
      unsigned _out_size,
      unsigned _nblocks
    );

    void setup (dtype* _output);

    void operator() (dtype* _input, dtype* _filter_coeff, dtype* _output);

  private:

    typename type_map::plan_type plan;

    void apply_filter (
      dtype* input,
      dtype* filter_coeff,
      dtype* output
    );

  };
}


#endif
