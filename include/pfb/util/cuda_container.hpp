#ifndef __cuda_container_hpp
#define __cuda_container_hpp

#include <complex>
#include <vector>

#include <cuda_runtime.h>
#include <cufft.h>


namespace pfb {
namespace util {

  template<typename dtype>
  struct cufft2std {};

  template<>
  struct cufft2std<cufftComplex> {
    typedef std::complex<float> type;
  };

  template<>
  struct cufft2std<cufftDoubleComplex> {
    typedef std::complex<double> type;
  };

  template<typename dtype>
  class CUDAContainer {
  public:

    CUDAContainer (unsigned size);

    CUDAContainer (const std::vector<typename cufft2std<dtype>::type>& data);

    CUDAContainer (const typename cufft2std<dtype>::type* data, unsigned size);

    ~CUDAContainer ();

    dtype* data () { return device_data; }

    unsigned size () { return nelements; }

    void h2d (const typename cufft2std<dtype>::type* data, unsigned size);

    void h2d (const std::vector<typename cufft2std<dtype>::type>& data);

    void d2h (typename cufft2std<dtype>::type* data);

    void d2h (std::vector<typename cufft2std<dtype>::type>& data);


  private:
    dtype* device_data;
    unsigned nelements;

  };
}
}

template<typename dtype>
pfb::util::CUDAContainer<dtype>::CUDAContainer (
  unsigned size
) : nelements(size)
{
  cudaMalloc((void **) &device_data, sizeof(dtype) * nelements);
}


template<typename dtype>
pfb::util::CUDAContainer<dtype>::CUDAContainer (
  const std::vector<typename cufft2std<dtype>::type>& data
)
{
  nelements = data.size();
  cudaMalloc((void **) &device_data, sizeof(dtype) * nelements);
  h2d(data);
}
template<typename dtype>
pfb::util::CUDAContainer<dtype>::CUDAContainer (
  const typename cufft2std<dtype>::type* data, unsigned size
)
{
  nelements = size;
  cudaMalloc((void **) &device_data, sizeof(dtype) * nelements);
  h2d(data, size);
}


template<typename dtype>
pfb::util::CUDAContainer<dtype>::~CUDAContainer ()
{
  cudaFree(device_data);
}


template<typename dtype>
void pfb::util::CUDAContainer<dtype>::h2d (
  const typename cufft2std<dtype>::type* data, unsigned size
)
{
  if (size > nelements) {
    cudaFree(device_data);
    nelements = size;
    cudaMalloc((void **) &device_data, sizeof(dtype) * nelements);
  }

  cudaMemcpy(
    device_data,
    (dtype*) data,
    nelements*sizeof(dtype),
    cudaMemcpyHostToDevice);
}



template<typename dtype>
void pfb::util::CUDAContainer<dtype>::h2d (
  const std::vector<typename cufft2std<dtype>::type>& data
)
{
  h2d(data.data(), data.size());
}

template<typename dtype>
void pfb::util::CUDAContainer<dtype>::d2h (
  typename cufft2std<dtype>::type* data
)
{
  cudaMemcpy(
    (dtype*) data,
    device_data,
    nelements*sizeof(dtype),
    cudaMemcpyDeviceToHost);
}



template<typename dtype>
void pfb::util::CUDAContainer<dtype>::d2h (
  std::vector<typename cufft2std<dtype>::type>& data
)
{
  if (data.size() < nelements) {
    data.resize(nelements);
  }
  d2h(data.data());
}

#endif
