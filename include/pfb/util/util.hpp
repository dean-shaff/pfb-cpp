#ifndef __util_hpp
#define __util_hpp

#include <tuple>
#include <string>
#include <complex>

namespace pfb {
namespace util {

  template<typename T>
  struct complex2float {};

  template<>
  struct complex2float<std::complex<float>>
  {
    typedef float type;
  };

  template<>
  struct complex2float<std::complex<double>>
  {
    typedef double type;
  };


  typedef std::tuple<unsigned, unsigned, unsigned, unsigned> four_tuple;

  struct rational {
    unsigned nu;
    unsigned de;

    rational () {
      nu = 1;
      de = 1;
    }

    rational (unsigned _nu, unsigned _de) {
      nu = _nu; de = _de;
    }
    
    bool operator==(const rational& rhs) const {
      return rhs.nu == nu && rhs.de == de;
    }

  };

  unsigned calc_filter_taps_padded (
    unsigned channels,
    unsigned filter_taps);

  four_tuple calc_out_size (
    unsigned channels,
    const rational& os_factor,
    unsigned in_size,
    unsigned filter_taps);

  void check_error (const std::string& msg);

}
}

#endif
