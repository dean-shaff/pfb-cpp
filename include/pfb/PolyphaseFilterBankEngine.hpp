#ifndef __PolyphaseFilterBankEngine_hpp
#define __PolyphaseFilterBankEngine_hpp

#include "util/util.hpp"

namespace pfb {

template<typename dtype>
class PolyphaseFilterBankEngine {
public:

  PolyphaseFilterBankEngine (
    unsigned _channels,
    pfb::util::rational _os_factor,
    unsigned _in_size,
    unsigned _filter_taps,
    unsigned _step,
    unsigned _filter_taps_padded,
    unsigned _out_size,
    unsigned _nblocks
  ):
  channels(_channels),
  os_factor(_os_factor),
  in_size(_in_size),
  filter_taps(_filter_taps),
  step(_step),
  filter_taps_padded(_filter_taps_padded),
  out_size(_out_size),
  nblocks(_nblocks)
  {
    is_setup = false;
  }

  virtual ~PolyphaseFilterBankEngine () {}

  virtual void setup (dtype* _output) = 0; // need the pointers for setting up FFT plans

  virtual void operator () (
    dtype* _input, dtype* _filter_coeff, dtype* _output) = 0;

protected:

  unsigned channels;
  pfb::util::rational os_factor;
  unsigned in_size;
  unsigned filter_taps;

  unsigned step;
  unsigned filter_taps_padded;
  unsigned out_size;
  unsigned nblocks;

  bool is_setup;


};

}

#endif
