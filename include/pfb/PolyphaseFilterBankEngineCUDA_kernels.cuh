#ifndef __PolyphaseFilterBankEngineCUDA_kernels_cuh
#define __PolyphaseFilterBankEngineCUDA_kernels_cuh

#define FULL_MASK 0xffffffff

__device__ unsigned roll (unsigned idx, unsigned size, unsigned roll_by);

template<typename T>
__global__ void k_print_array(const T* arr, unsigned size);

template<typename T>
__device__ T warp_reduce_sum_complex(T val);

template<typename T>
__global__ void k_apply_filter_reduce_sum (
  T* in,
  T* out,
  T* filter_coeff,
  unsigned filter_taps,
  unsigned step,
  unsigned downsample,
  unsigned nblocks,
  unsigned filter_taps_per_sample
);

template<typename T>
__global__ void k_apply_filter (
  T* in,
  T* out,
  T* filter_coeff,
  unsigned filter_taps,
  unsigned step,
  unsigned downsample,
  unsigned nblocks,
  unsigned filter_taps_per_sample
);

#endif
