import os

from distutils.core import Extension

from distutils.command.build import build as build_base


class CustomBuild(build_base):
    sub_commands = [
        ('build_ext', build_base.has_ext_modules),
        ('build_py', build_base.has_pure_modules),
        ('build_clib', build_base.has_c_libraries),
        ('build_scripts', build_base.has_scripts),
    ]


base_dir = os.path.dirname(os.path.abspath(__file__))

backends_include_dirs = [
    os.path.join(base_dir, "include"),
    os.path.join(base_dir, "src")
]

backends_libraries = ["fftw3f", "fftw3"]

swig_opts = ["-py3", "-c++"]
swig_opts.extend([f"-I{p}" for p in backends_include_dirs])


swig_extension = Extension(
    "pfb._PFBPlan", [
        os.path.join(base_dir, "python", "pfb", "PFBPlan.i"),
        os.path.join(base_dir, "src", "PolyphaseFilterBankEngineCPU.cpp"),
        os.path.join(base_dir, "src", "util.cpp")
    ],
    swig_opts=swig_opts,
    include_dirs=backends_include_dirs,
    libraries=backends_libraries
)


extensions = [
    swig_extension
]


def build(setup_kwargs):
    """
    This function is mandatory in order to build the extensions.
    """
    # print(os.listdir())
    # print(os.listdir("python"))
    # print(os.listdir("src"))
    # with open("setup.py", "r") as fd:
    #     print(fd.read())
    setup_kwargs.update(
        {
            "ext_modules": extensions,
            "package_dir": {
                "": "python"
            },
            "py_modules": ["pfb.PFBPlan"],
            "cmdclass": {'build': CustomBuild}
        }
    )
