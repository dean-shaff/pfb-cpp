function generate_test_vector(os_factor, channels, blocks, taps_per_channel)

  % generate random numbers, but make sure to use the same stream.
  s = RandStream('mrg32k3a', 'Seed', 1234);
  RandStream.setGlobalStream(s);

  % os_factor = struct('nu', 8, 'de', 7);
  % channels = 128;
  % blocks = 50;
  % taps = channels*10 + 1;
  taps = channels * taps_per_channel + 1;

  step = os_factor.de * channels / os_factor.nu
  taps_padded = taps + channels - 1;
  in_size = blocks * step + taps_padded;

  filter = randn(taps, 1);
  indat = randn(1, 1, in_size) + 1j*randn(1, 1, in_size);

  outdat = polyphase_analysis(...
    indat, filter, channels, os_factor);
  write_pfb_file(...
    channels, os_factor, taps, blocks, './../test/test_data/polyphase_analysis', outdat);

  % outdat_padded = polyphase_analysis_padded(indat, filter, channels, os_factor);
  % write_pfb_file(channels, os_factor, taps, blocks, 'polyphase_analysis_padded', outdat);
  write_input_file(in_size, './../test/test_data/input', indat);
  write_filter_file(channels, os_factor, taps, './../test/test_data/filter', filter);

end

function write_complex(fid, arr)
  arr_size = size(arr);
  arr_size = arr_size(1);

  arr_flat = zeros(2*arr_size, 1);

  arr_flat(1:2:end) = real(arr);
  arr_flat(2:2:end) = imag(arr);

  fwrite(fid, arr_flat, 'double');

end


function write_input_file (in_size, file_prefix, input_dat)
  file_name = sprintf('%s.size_%d.dat',...
    file_prefix, in_size);

  fid = fopen(file_name, 'w');

  write_complex(fid, squeeze(input_dat));

  fclose(fid);
end


function write_filter_file (channels, os_factor, taps, file_prefix, filter)
  file_name = sprintf('%s.channels_%d.nu_%d-de_%d.taps_%d.dat',...
    file_prefix, channels, os_factor.nu, os_factor.de, taps);

  fid = fopen(file_name, 'w');

  filter = filter + 1j*zeros(taps, 1);
  write_complex(fid, filter);

  fclose(fid);

end


function write_pfb_file (channels, os_factor, taps, blocks, file_prefix, outdat)

  file_name = sprintf('%s.channels_%d.nu_%d-de_%d.taps_%d.blocks_%d.dat',...
    file_prefix, channels, os_factor.nu, os_factor.de, taps, blocks);

  fid = fopen(file_name, 'w');

  outdat_flat = reshape(squeeze(outdat), blocks*channels, 1);

  write_complex(fid, outdat_flat);

  fclose(fid);

end
