cmake_minimum_required(VERSION 3.1)
set(CMAKE_CXX_STANDARD 11)

file(STRINGS version.txt PACKAGE_VERSION)

project(pfb VERSION ${PACKAGE_VERSION} LANGUAGES CXX)

include(CheckLanguage)
check_language(CUDA)

if (CMAKE_CUDA_COMPILER)
  enable_language(CUDA)
	set(HAVE_CUDA 1)
else()
  message(STATUS "No CUDA compiler found")
  set(HAVE_CUDA 0)
endif()

set(INCLUDE_DIR ${CMAKE_SOURCE_DIR}/include)
set(SOURCE_DIR ${CMAKE_SOURCE_DIR}/src)
set(TEST_DIR ${CMAKE_SOURCE_DIR}/test)
set(BENCH_DIR ${CMAKE_SOURCE_DIR}/bench)

set(FFTW_DIR "" CACHE STRING "FFTW directory")
set(DEBUG 0 CACHE STRING "debug mode")

set(CMAKE_CXX_FLAGS "-Wall -Wextra -Wshadow -Wnon-virtual-dtor -pedantic -fopenmp")
set(CMAKE_CUDA_FLAGS "-std=c++11")

find_path(FFTW_INCLUDE "fftw3.h" HINTS ${FFTW_DIR}/include)
find_library(FFTW_LIBRARY fftw3 HINTS ${FFTW_DIR}/lib)
find_library(FFTWF_LIBRARY fftw3f HINTS ${FFTW_DIR}/lib)

message(STATUS "FFTW_INCLUDE=${FFTW_INCLUDE}")
message(STATUS "FFTW_LIBRARY=${FFTW_LIBRARY}")
message(STATUS "FFTWF_LIBRARY=${FFTWF_LIBRARY}")

set(PFB_LIB
  pfb-lib
)

set(PFB_LIB_LINK_LIBRARIES
  ${FFTW_LIBRARY}
  ${FFTWF_LIBRARY}
)

set(PFB_LIB_INCLUDE_DIRECTORIES
  ${INCLUDE_DIR}
  ${FFTW_INCLUDE}
)

set(PFB_LIB_SOURCES
  ${SOURCE_DIR}/util.cpp
  ${SOURCE_DIR}/PolyphaseFilterBankEngineCPU.cpp
)


if (HAVE_CUDA)

  set(PFB_LIB_SOURCES
    ${PFB_LIB_SOURCES}
    ${SOURCE_DIR}/PolyphaseFilterBankEngineCUDA_kernels.cu
    ${SOURCE_DIR}/PolyphaseFilterBankEngineCUDA.cu
  )

  find_library(CUFFT_LIBRARY cufft HINTS ${CMAKE_CUDA_IMPLICIT_LINK_DIRECTORIES})
  find_library(CUDART_LIBRARY cudart HINTS ${CMAKE_CUDA_IMPLICIT_LINK_DIRECTORIES})
  message(STATUS "CUFFT_LIBRARY=${CUFFT_LIBRARY}")
  message(STATUS "CUDART_LIBRARY=${CUDART_LIBRARY}")

  set(PFB_LIB_LINK_LIBRARIES
    ${PFB_LIB_LINK_LIBRARIES}
    ${CUFFT_LIBRARY}
    ${CUDART_LIBRARY}
  )

  set(PFB_LIB_INCLUDE_DIRECTORIES
    ${PFB_LIB_INCLUDE_DIRECTORIES}
    ${CMAKE_CUDA_TOOLKIT_INCLUDE_DIRECTORIES}
  )

endif()

add_library(${PFB_LIB} SHARED ${PFB_LIB_SOURCES})
target_include_directories(${PFB_LIB} PUBLIC ${PFB_LIB_INCLUDE_DIRECTORIES})
target_link_libraries(${PFB_LIB} PUBLIC ${PFB_LIB_LINK_LIBRARIES})
target_compile_definitions(${PFB_LIB} PRIVATE VERBOSE=${DEBUG} HAVE_CUDA=${HAVE_CUDA})
target_compile_options(${PFB_LIB} PUBLIC -O3)
set_target_properties(${PFB_LIB}
  PROPERTIES OUTPUT_NAME pfb
)

add_subdirectory(src)
add_subdirectory(python)
add_subdirectory(test)
